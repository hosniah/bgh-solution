CREATE TABLE IF NOT EXISTS `order_products_client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_commande` int(11) NOT NULL,
  `reference` varchar(256) NOT NULL,
  `quantite` int(11) NOT NULL,
  `commentaires` varchar(512) NOT NULL,
  `id_pointVente` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

INSERT INTO `order_products_client` (`id`, `id_commande`, `reference`, `quantite`, `commentaires`, `id_pointVente`, `status`) VALUES
(1, 1, '58252',30,'',8,100),
(2, 1, '57100',1,'',8,100),
(3, 1, '56569',20,'',8,100),
(4, 1, '55948',1919,'',8,100),
(5, 1, '58252',30,'',8,100),
(6, 1, '57100',1,'',8,100),
(7, 1, '56569',10,'',8,100),
(8, 1, '55948',2919,'',8,100);


