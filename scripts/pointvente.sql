-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 31, 2016 at 10:18 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bgh-solution-old`
--

-- --------------------------------------------------------

--
-- Table structure for table `pointvente`
--

CREATE TABLE IF NOT EXISTS `pointvente` (
  `id_pv` int(11) NOT NULL AUTO_INCREMENT,
  `label_pv` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `tel_pv` int(11) DEFAULT NULL,
  `fax_pv` int(11) DEFAULT NULL,
  `adresse_pv` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `email_pv` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `resp_pv` int(11) NOT NULL,
  `deleted_pv` int(11) NOT NULL,
  PRIMARY KEY (`id_pv`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `pointvente`
--

INSERT INTO `pointvente` (`id_pv`, `label_pv`, `tel_pv`, `fax_pv`, `adresse_pv`, `email_pv`, `resp_pv`, `deleted_pv`) VALUES
(1, 'boutik hamamlif', 71217000, 71217000, '1 rue de la liberté hamam-lif', 'boutikhmlif@gmail.com', 7, 0),
(2, 'boutik commession', 71100000, 71100000, '1 rue commession Tunis', 'boutikcommession@gmail.com', 6, 0),
(3, 'boutik laouina', 71200000, 71200000, '1 rue des fleurs laouina', 'boutiklaouina@gmail.com', 8, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
