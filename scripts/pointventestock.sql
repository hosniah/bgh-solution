-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 31, 2016 at 10:44 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bgh-solution`
--

-- --------------------------------------------------------

--
-- Table structure for table `pointventestock`
--

CREATE TABLE IF NOT EXISTS `pointventestock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_importateur` int(11) NOT NULL DEFAULT '1',
  `nsFamille` int(11) NOT NULL,
  `reference` varchar(30) DEFAULT NULL,
  `codeBarre` varchar(30) DEFAULT NULL,
  `designation` varchar(50) DEFAULT NULL,
  `prixGrosHT` varchar(30) DEFAULT NULL,
  `tva` int(11) DEFAULT NULL,
  `unite` int(11) DEFAULT NULL,
  `colisageCtn` int(11) DEFAULT NULL,
  `emballage` varchar(50) DEFAULT NULL,
  `prixHlif` varchar(30) DEFAULT NULL,
  `prixTunis` varchar(30) DEFAULT NULL,
  `ctnDispo` int(11) DEFAULT NULL,
  `pcsDispo` int(11) DEFAULT NULL,
  `stockPcs` int(11) DEFAULT NULL,
  `cf` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `image` varchar(30) DEFAULT NULL,
  `id_pointvente` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin2 AUTO_INCREMENT=239 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
