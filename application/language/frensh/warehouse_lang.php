<?php

// header

$lang['header_title'] = 'Entrepôt par Encrypted.pl';
$lang['header_desc'] = 'Entrepôt par Encrypted.pl';
$lang['header_keywords'] = 'Entrepôt par Encrypted.pl';

// users
$lang['user_1'] = 'Worker';
$lang['user_2'] = 'Admin';
$lang['user_3'] = 'Superviseur';
$lang['user_4'] = 'Gérant';
$lang['user_5'] = 'Superadmin';
$lang['user_6'] = 'Responsable dépot';

// footer
$lang['footer_text'] = "&copy; Encrypted.pl Warehouse";

// menu
$lang['menu_menu'] = "Menu";
$lang['menu_production'] = "Production";
$lang['menu_proreport'] = "Rapport de Production";
$lang['menu_inproduction'] = "Ajout de production";
$lang['menu_add_packing'] = "En chemin";
$lang['menu_packing_transport'] = "Ajout liste de colisage";
$lang['menu_group_packing_transport'] = "Groupe ajoutant liste de colisage";
$lang['menu_grouppacking_warehouse'] = "Liste de colisage d'ajout automatique";
$lang['menu_warehouse'] = "Entrepôt";
$lang['menu_warehouse_in'] = "Emballage revenus de liste";
$lang['menu_warehouse_groupin'] = "Liste de colisage revenus du groupe";
$lang['menu_warehouse_back'] = "Revenir";
$lang['menu_warehouse_correct'] = "Correction";
$lang['menu_warehouse_out'] = "Résultat";
$lang['menu_warehouse_report'] = "Rapport d'Entrepôt";
$lang['menu_lib'] = "Bibliothèque";
$lang['menu_lib_products'] = "Produits";
$lang['menu_lib_packing'] = "listes de colisage";
$lang['menu_lib_orders'] = "Commandes";
$lang['menu_users'] = "Utilisateurs";
$lang['menu_manage'] = "Faire en sorte";
$lang['menu_system'] = "Système";
$lang['menu_system_log'] = "System log";
$lang['menu_system_notes'] = "Notes";
$lang['menu_system_zero'] = "Système de compensation";
$lang['menu_system_search'] = "Recherche de Système";
$lang['menu_login'] = "Connexion";
$lang['menu_login_user'] = "Utilisateur: ";
$lang['menu_login_logout'] = "Se déconnecter";
$lang['menu_login_login'] = "Login:";
$lang['menu_login_passwd'] = "Password:";
$lang['menu_login_signin'] = "Connexion";
$lang['menu_backup'] = "Sauvegarde";
$lang['menu_famille'] = "Famille";
$lang['menu_list_famille'] = "Liste famille";
$lang['menu_list_sous_famille'] = "Sous famille";
$lang['menu_adding_famille'] = "Ajout famille";
// h1

//$lang['h1_add_product_production'] = "Adding products to production";
$lang['h1_add_product_production'] = "Ajout de quantité d'articles au stock";
$lang['h1_add_packing_onway'] = "Ajout liste de colisage sur le chemin";
$lang['h1_add_packing_grouponway'] = "Groupe ajoutant liste de colisage en chemin";
$lang['h1_add_packing_warehouse'] = "Ajout liste de colisage à l'entrepôt";
$lang['h1_add_grouppacking_warehouse'] = "Groupe ajoutant liste d'emballage à l'entrepôt";
$lang['h1_add_autopacking_warehouse'] = "Ajout automatique liste d'emballage à l'entrepôt";
$lang['h1_add_return'] = "Adding product - return";
$lang['h1_add_correction'] = "Correction";
$lang['h1_add_sent'] = "Résultat du produit";
$lang['h1_add_product'] = "Ajout d'un nouveau produit";
$lang['h1_lib_manage'] = "Gestion des produits";
$lang['h1_add_packing'] = "Ajout liste de colisage";
$lang['h1_add_order'] = "Ajout de l'ordre";
$lang['h1_users_manage'] = "Gestion des utilisateurs";
$lang['h1_users_manage_add'] = "Ajout ou modification d'utilisateur";
$lang['h1_notes_manage'] = "Remarques managment";
$lang['h1_notes_manage_add'] = "Ajout ou modification des notes";
$lang['h1_report'] = "Actions de journal système";
$lang['h1_proreport'] = "Journal de production";
$lang['h1_status'] = "Le statut de l'entrepôt actuel";
$lang['h1_search'] = "Recherche";
$lang['h1_noaccess'] = "Pas d'accès";
$lang['h1_id_famille'] = "Identifiant";
$lang['h1_label_famille'] = "Famille";
$lang['h1_code_famille'] = "Code famille";
$lang['h1_status_famille'] = "Status";
$lang['h1_action_famille'] = "Action";
$lang['h1_label_sous_famille'] = "Sous famille";
// h2

$lang['h2_zero_all'] = "Supprimer des valeurs numériques";
$lang['h2_zero_all_info'] = "Les valeurs numériques supprimés!!!";
$lang['h2_zero_pro'] = "Supprimer produits";
$lang['h2_zero_pro_info'] = "Produits supprimés!!!";
$lang['h2_zero_desc'] = "Supprimer la description donnée dans la production";
$lang['h2_zero_desc_info'] = "Numérique pour la description supprimé dans la production!!!";
$lang['h2_zero_desc_error_info'] = "Numérique pour la description pas supprimés dans la production!!!";

// global strings

$lang['product'] = "Article";
$lang['order'] = "Ordre";
$lang['packing'] = "Liste de colisage";
$lang['amount'] = "Quantité";
$lang['id'] = "Id";
$lang['desc'] = "Description";
$lang['name'] = "Nom";
$lang['actions'] = "Actions";
$lang['login'] = "Utilisateur";
$lang['level'] = "Niveau";
$lang['passwd'] = "Password";
$lang['data'] = "Date";
$lang['data_start'] = "Date de début";
$lang['data_end'] = "Date de fin";
$lang['title'] = "Titre";
$lang['text'] = "Content";
$lang['new'] = "Nouveau ajout";
$lang['edit'] = "Editer";
$lang['confirm_delete'] = "'Vous confirmer la suppression?'";
$lang['del'] = "Supprimer";
$lang['generate'] = "Generate";
$lang['submit'] = "Envoyer Query";
$lang['lib'] = "Librairie";
$lang['wh'] = "Entrepôt";
$lang['onway'] = "En chemin";
$lang['production'] = "In production";
$lang['total'] = "Total";
$lang['given'] = "Total donné";
$lang['all'] = "Tous";
$lang['all_desc'] = "Toutes descriptions";
$lang['confirm'] = "Confirmer";
$lang['print'] = "Imprimer";
$lang['print_csv'] = "Imprimer CSV";
$lang['backup_info'] = "Vous allez faire sauvegarde complète de cette base de données MySQL de l'entrepôt, vous pouvez restaurer cette sauvegarde à tout moment, par l'exécution de script SQL sur votre base de données téléchargé.";

// actions

$lang['action_1'] = 'Changement de production';
$lang['action_2'] = 'Redonner entrepôt';
$lang['action_3'] = 'Correction entrepôt';
$lang['action_4'] = 'Abandonner entrepôt';
$lang['action_5'] = 'Changement sur la liste de colisage';
$lang['action_6'] = 'ajouter l\'emballage à l\'entrepôt';

// long txt

$lang['long_noaccess'] = 'Vous n\'êtes pas autorisé à afficher cette page!';
