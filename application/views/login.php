<?php $this->load->view('template/header_beta_view.php'); ?>

<body>
    <div class="container login">
	
	<?php
	    echo '<br><br>';
		echo '<center><img height="480" width="480" src="'.base_url().'images/logo.jpg" /></center>';
	
		if ($this->session->userdata('user_id')) { ?>
									<?php echo $this->lang->line('menu_login_user'); ?><b><?php echo $this->session->userdata('user_login'); ?></b>	
									<form action="" method="POST" class='form-signin'>
										<input type="submit" name="logout" value="<?php echo $this->lang->line('menu_login_logout'); ?>">
									</form>

								<?php } else { ?>
									<form action="" method="POST">
										<?php echo $this->lang->line('menu_login_login'); ?>
										<input type="text" name="login">
										<?php echo $this->lang->line('menu_login_passwd'); ?>
										<input type="password" name="passwd"><br>
										<a href="">Mot de passe oublié</a><br><br>
										<button type="submit" class="btn btn-outline btn-primary btn-lg btn-block" > <?php echo $this->lang->line('menu_login_signin'); ?> </button>
									</form>
								<?php } ?>
     
    </div><!--container-->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  </body>
</html>    
    