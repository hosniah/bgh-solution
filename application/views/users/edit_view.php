<?php $this->load->view('template/header_beta_view.php'); ?>

<body>
<script src="<?php echo base_url();?>js/featherlight/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
<link href="<?php echo base_url();?>js/featherlight/release/featherlight.min.css" type="text/css" rel="stylesheet" />
<div id="wrapper">
<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
<?php
    $this->load->view('template/topbar');
    $this->load->view('template/sidebar');
?>
</nav>
<div id="page-wrapper">
<div class="row">
<!-- main content area -->  
<!-- main content area -->
<div class="col-lg-12">
<div class="panel-body">
<div class="dataTable_wrapper">
<?php if (isset($error)): ?>
    <div class="alert alert-error"><?php echo $error; ?></div>
<?php endif; ?>
<?php if ($this->session->flashdata('success') == TRUE): ?>
        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
<?php endif; ?>
<section id="page-header" class="clearfix">    
	<div class="wrapper">
		<h1><?php echo $this->lang->line('h1_users_manage_add'); ?></h1>
    </div>

</section>


<!-- main content area -->   
<div class="wrapper" id="main"> 

	<!-- content area -->    
	<section id="content" class="wide-content">
		<div class="grid_4">
		<form action="" method="POST">
			<input type="hidden" name="sent" value="yes">
			<div class="label"><?php echo $this->lang->line('login'); ?></div>
			<input type="text" name="login" value="<?php	if (isset($user['login'])) {	echo $user['login']; } ?>" required>
			<div class="label"><?php echo $this->lang->line('passwd'); ?></div>
			<?php if (!isset($user['login'])) {	?>
				<input type="password" name="passwd" value="">
			<?php }	?>
			
			<div class="label"><?php echo $this->lang->line('level'); ?></div>
			<select name="level">						
				<option value="1" <?php
				if (isset($user['level']) && ($user['level'] == 1)) {
					echo 'selected="true"';
				}
				?>><?php echo "Super administrateur"; ?></option>
				<option value="2" <?php
				if (isset($user['level']) && ($user['level'] == 2)) {
					echo 'selected="true"';
				}
				?>><?php echo "Administrateur"; ?></option>
                                <option value="3" <?php
				if (isset($user['level']) && ($user['level'] == 3)) {
					echo 'selected="true"';
				}
				?>><?php echo "Responsable dépôt"; ?></option>
                                <option value="4" <?php
				if (isset($user['level']) && ($user['level'] == 4)) {
					echo 'selected="true"';
				}
				?>><?php echo "Gérant"; ?></option>
                                <option value="5" <?php
				if (isset($user['level']) && ($user['level'] == 5)) {
					echo 'selected="true"';
				}
				?>><?php echo "Super vendeur"; ?></option>
                                <option value="6" <?php
				if (isset($user['level']) && ($user['level'] == 6)) {
					echo 'selected="true"';
				}
				?>><?php echo "Vendeur"; ?></option>
			</select>
			<br><br>
			<select name="idPointVente">
			<option value="">--- Choisir ---</option>
                         <?php
                            foreach ($pointvente as $row) {
                                if($row['id_pv'] == $user['id_pointvente'])
                                    echo "<option value=".$row['id_pv']." selected> ".$row['label_pv']." </option>";
                                else
                                    echo "<option value=".$row['id_pv']." > ".$row['label_pv']." </option>";
                            }
                        ?>
			</select>	
			<br><br>
			<input type="submit" value="<?php echo $this->lang->line('submit'); ?>">
		</form>
		</div>
	</section><!-- #end content area -->

</div><!-- #end div #main .wrapper -->

</div>
</div>
</div>


</div></div>

</div><!-- #end div #main .wrapper -->
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/jquery/dist/jquery.min.js"></script>
<link type="text/css" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.8.20.custom.css" rel="Stylesheet" />
<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/jquery-ui.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
$(document).ready(function() {
    $('#example').DataTable({
        responsive: true,
        "order": [[ 1, "desc" ]]
    });
});
</script>
</html>
<?php // $this->load->view('template/footer_view_datatables.php'); ?>
