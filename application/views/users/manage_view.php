<?php $this->load->view('template/header_beta_view.php'); ?>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
			<?php 
				$this->load->view('template/topbar');
				$this->load->view('template/sidebar');
			?>

        </nav>
        <div id="page-wrapper">
		
<!-- main content area -->   
<div class="wrapper" id="main"> 
<section id="page-header" class="clearfix">    
	<div class="wrapper">
		<h1><?php echo $this->lang->line('h1_users_manage'); ?></h1>
    </div>

</section>
	<!-- content area -->    
	<section id="content" class="wide-content">
		<a href="<?php echo base_url(); ?>users/manage/edit/0" class="btn btn-warning"><?php echo 'Ajouter un nouvel utilisateur';//echo $this->lang->line('new'); ?></a>
		<table>
			<tr><!--<th><?php //echo $this->lang->line('id');     ?></th>-->
				<th><?php echo $this->lang->line('login'); ?></th>
				<th><?php echo $this->lang->line('level'); ?></th>
				<th><?php echo $this->lang->line('actions'); ?></th></tr>
			<?php $i = 1; ?>
			<?php foreach ($users as $user) { ?>
				<tr>
					<!--<td><?php //echo $i++;     ?></td>-->
					<td><b><?php echo $user['login']; ?><b></td>
								<td>
									<?php
									if ($user['level'] == 1) {
										echo "Super administrateur";
									}
									?>
									<?php
									if ($user['level'] == 2) {
										echo "Administrateur";
									}
									?>
									<?php
									if ($user['level'] == 3) {
										echo "Responsable dépôt";
									}
									?>
									<?php
									if ($user['level'] == 4) {
										echo "Gérant";
									}
									?>
									<?php
									if ($user['level'] == 5) {
										echo "Super vendeur";
									}
									?>
									<?php
									if ($user['level'] == 6) {
										echo "Vendeur";
									}
									?>
								</td>
								<td>
									<a href="<?php echo base_url(); ?>users/manage/edit/<?php echo $user['id']; ?>"  class="btn btn-success" ><?php echo $this->lang->line('edit'); ?></a>
									<a href="<?php echo base_url(); ?>users/manage/del/<?php echo $user['id']; ?>" class="btn btn-danger" onclick="return confirm(<?php echo $this->lang->line('confirm_delete'); ?>);"><?php echo $this->lang->line('del'); ?></a>
								</td>
								</tr>
							<?php } ?>			
							</table>
							</section><!-- #end content area -->

							</div><!-- #end div #main .wrapper -->

							
</div>
</div>
</div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/jquery/dist/jquery.min.js"></script>
		<link type="text/css" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.8.20.custom.css" rel="Stylesheet" />
		<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/jquery-ui.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/metisMenu/dist/metisMenu.min.js"></script>
    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/dist/js/sb-admin-2.js"></script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true,
				 "order": [[ 1, "desc" ]]
        });
    });
    </script>

</html>