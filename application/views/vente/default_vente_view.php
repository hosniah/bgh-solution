<?php $this->load->view('template/header_beta_view.php'); ?>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
			<?php 
				$this->load->view('template/topbar');
				$this->load->view('template/sidebar');
			?>
        </nav>
<!-- main content area -->   


<div id="page-wrapper">
<div class="row"> 
<br><div class="col-lg-12">
<div class="panel panel-default">
		<div class="panel-heading">Nouvelle vente En Gros</div><br>

 <div class="panel-body">

                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $countVenteGros; ?></div>
                                    <div>Ventes En Gros!</div>
                                </div>
                            </div>
                        </div>
                        <a href="<?php echo base_url()."vente/vente/venteGros";?>">
                            <div class="panel-footer">
                                <span class="pull-left">Nouvelle vente en gros</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
	</div>
</div>

<div class="panel panel-default">

<div class="panel-heading">Nouvelle vente détails</div><br>
	<div class="panel-body">


  
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-shopping-cart fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">124</div>
                                    <div>Ventes Détails!</div>
                                </div>
                            </div>
                        </div>
                        <a href="<?php echo base_url()."vente/vente/venteDetails";?>">
                            <div class="panel-footer">
                                <span class="pull-left">Nouvelle vente - Saisie par douchette</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>



                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-shopping-cart fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $countVenteManuelle; ?></div>
                                    <div>Ventes Détails!</div>
                                </div>
                            </div>
                        </div>
                        <a href="<?php echo base_url()."vente/vente/venteDetailsManuelle";?>">
                            <div class="panel-footer">
                                <span class="pull-left">Nouvelle vente - Saisie manuelle</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
</div>

</div>
</div>
</div>
</div><!-- #end div #main .wrapper -->
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/jquery/dist/jquery.min.js"></script>
		<link type="text/css" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.8.20.custom.css" rel="Stylesheet" />
		<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/jquery-ui.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/metisMenu/dist/metisMenu.min.js"></script>
    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/dist/js/sb-admin-2.js"></script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#example').DataTable({
                responsive: true,
				 "order": [[ 1, "desc" ]]
        });
    });
    </script>
</html>