<?php $this->load->view('template/header_beta_view.php'); ?>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
			<?php 
				$this->load->view('template/topbar');
				$this->load->view('template/sidebar');
			?>
        </nav>
<!-- main content area -->   


<div id="page-wrapper">
<div class="row"> 
<br>
               <!-- /.col-lg-6 -->
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading"> Ventes GROS/Détails </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="morris-bar-chart"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading"> Ventes par familles</div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="morris-donut-chart"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
<div class="col-lg-12">
<div class="panel panel-default">
		<div class="panel-heading">Liste des ventes</div><br>

 <div class="panel-body">

     <div class="dataTable_wrapper">

            <table id="example" class="" cellspacing="0" width="90%">
                <thead><tr><th>#</th><th>Type</th><th>Montant</th><th>Date</th>	</tr>
				</thead>
                <tbody>
				<?php
                if (empty($vente)){
                    echo '   <tr><td colspan="4">Aucune vente pour le moment</td></tr>';
                } else {                    
                    foreach ($vente as $key => $row) {
						if ($row['type_vente'] == 1){ $type = "Gros";} else{ $type = "Détail";}
						echo'<tr>
								<td>'. $row['sale_id'].'</td>
								<td>'. $type.'</td>
								<td>'. $row['sum'].'</td>
								<td>'. $row['time'].'</td>
							<tr>';
					}
					
				}
				?>
				</tbody>
            </table>



                

</div>
</div>

</div>
</div>
</div>
</div><!-- #end div #main .wrapper -->
    <!-- /#wrapper -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/jquery/dist/jquery.min.js"></script>
		<link type="text/css" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.8.20.custom.css" rel="Stylesheet" />
		<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/jquery-ui.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/metisMenu/dist/metisMenu.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/raphael/raphael-min.js"></script>
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/morrisjs/morris.min.js"></script>
     <!-- DataTables JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/dist/js/sb-admin-2.js"></script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
	/*
    $(document).ready(function() {
        $('#example').DataTable({
                responsive: true,
				 "order": [[ 1, "desc" ]]
        });
    });
	*/
	
$(function() {


    Morris.Bar({
        element: 'morris-bar-chart',
        data: [{
            y: '08/2015',
            a: 100,
            b: 90
        }, {
            y: '09/2015',
            a: 75,
            b: 65
        }, {
            y: '10/2015',
            a: 50,
            b: 40
        }, {
            y: '11/2015',
            a: 75,
            b: 65
        }, {
            y: '12/2015',
            a: 50,
            b: 40
        }, {
            y: '01/2016',
            a: 75,
            b: 65
        }, {
            y: '02/2016',
            a: 100,
            b: 90
        }],
        xkey: 'y',
        ykeys: ['a', 'b'],
        labels: ['Ventes  Gros', 'Ventes Détails'],
        hideHover: 'auto',
        resize: true
    });
    Morris.Donut({
        element: 'morris-donut-chart',
        data: [{
            label: "Jouets",
            value: 12
        }, {
            label: "BOUEES DE PLAGE",
            value: 18
        }, {
            label: "ARTICLES DE NOEL ET CARNAVAL",
            value: 30
        }, {
            label: "SPORT",
            value: 20
        }],
        resize: true
    });


});
    </script>
</html>