<?php $this->load->view('template/header_beta_view.php'); ?>

  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
  <?php
echo ' <script>
  $(function() {
    var availableTags = '.$products_list.';
    $( "#txtRef" ).autocomplete({
      source: availableTags
    });
  });
  
  function verifStock(reference){
  	if(reference) {
		var myref = reference.trim();
		if(myref) {
			//alert("hello: "+myref);
			$.post( "'.base_url().'warehouse/library/ajaxGetsotckByRef", { reference: myref })
			  .done(function( data ) {
				    document.getElementById("txtStock").value = data;
					document.getElementById("imgarticle").innerHTML = "<img height=\"180\" width=\"180\" src=\''.base_url().'images/products/"+myref+".jpg\' />";
			  });

			$.post( "'.base_url().'warehouse/library/ajaxGetPointDeVentesotckByRef", { reference: myref , id_pt_vente: "'.$this->session->userdata('id_pointvente').'"})
			  .done(function( data ) {
				    document.getElementById("txtStockPtVente").value = data;
			  });
		}
	}
  }

    function updatedQuantity(rowId) {
		var my_input1 =  document.getElementById("computed_total_ht").value;
		var my_input2 =  document.getElementById("total_"+rowId).value;
		var diff1 = parseInt(my_input1) - parseInt(my_input2);
        document.getElementById("computed_total_ht").value = diff1;

		var my_input3 =  document.getElementById("txtDu").value;
		var diff2 = parseInt(my_input3) - parseInt(my_input2);

        document.getElementById("txtDu").value = diff2;
        document.getElementById("total_"+rowId).value =  document.getElementById("quantity_"+rowId).value *  document.getElementById("price_"+rowId).value;

		var my_input1 =  document.getElementById("computed_total_ht").value;
		var my_input2 =  document.getElementById("total_"+rowId).value;
		var sum = parseInt(my_input1) + parseInt(my_input2);
        document.getElementById("computed_total_ht").value = sum;  
        document.getElementById("txtDu").value             = sum;  

		$.post( "'.base_url().'vente/vente/updateQuantity", { id_sales_detail: rowId,qte: document.getElementById("quantity_"+rowId).value})
		  .done(function( data ) {
		  });
	}

    function updatedPrice(rowId) {
			var my_input1 =  document.getElementById("computed_total_ht").value;
		var my_input2 =  document.getElementById("total_"+rowId).value;
		var diff1 = parseInt(my_input1) - parseInt(my_input2);
        document.getElementById("computed_total_ht").value = diff1;

		var my_input3 =  document.getElementById("txtDu").value;
		var diff2 = parseInt(my_input3) - parseInt(my_input2);

        document.getElementById("txtDu").value = diff2;
        document.getElementById("total_"+rowId).value =  document.getElementById("quantity_"+rowId).value *  document.getElementById("price_"+rowId).value;

		var my_input1 =  document.getElementById("computed_total_ht").value;
		var my_input2 =  document.getElementById("total_"+rowId).value;
		var sum = parseInt(my_input1) + parseInt(my_input2);
        document.getElementById("computed_total_ht").value = sum;  
        document.getElementById("txtDu").value             = sum;  
 
			$.post( "'.base_url().'vente/vente/updatePrice", { id_sales_detail: rowId,price: document.getElementById("price_"+rowId).value})
			  .done(function( data ) {
			  });
    }


	function verifReglement() {
		var txtdu = document.getElementById("txtDu").value;
		var txtEncaisse = document.getElementById("txtEncaisse").value;
		/* if (  txtDu  >=  txtEncaisse ) {
				alert("Le montant encaissé doit être dupérieur au total TTC.");
			return false;
		} else {
		*/
			return true;
	}
  </script>';
  ?>
<body>
    <div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
			<?php 
				$this->load->view('template/topbar');
				$this->load->view('template/sidebar');
			?>
    </nav>
<!-- main content area -->
<div id="page-wrapper">
<div class="row">
<br>

<div class="col-lg-6">
<div class="panel panel-default">
		<div class="panel-heading">Articles</div>
			<div class="panel-body">
						<form id="frmFacturation" class="formulaire1" method="post" action="venteGros" autocomplete="off">
							<input type="hidden" id="token" name="token" value="3fc894aba2cd321f33db13cff528838e">
							<table>
								<center><span  id="imgarticle" name="imgarticle">   </span></center>
								<tbody><tr><th class="label1">Search (Reférence)</th>
								<th class="label1">Code à barre</th>
								<!--<th class="label1">Description</th> -->
								</tr><tr><!-- Affichage de la reference et de la designation -->
								<td> <input class="texte_ref" type="text" id="txtRef" name="txtRef" value="" onblur="javascript: verifStock(this.value);" ></td>
								<td> <input class="texte_barcode" type="text" id="txtBarcode" name="txtBarcode" value=""></td>
								</tr>
								  <tr></tr>
							</tbody></table>
						<!-- </form> -->

						<!-- <form id="frmQte" class="formulaire1" method="post" action="facturation_verif.php?action=ajout_article" onsubmit="javascript: return verifSaisie();">
							<input type="hidden" name="token" value="3fc894aba2cd321f33db13cff528838e">
							-->
							<table>
								<tbody><tr><th>Quantité</th> <th>Stock Pt de vente</th><th>Stock Rades</th>
								<!-- <th>Prix unitaire (Hors taxes)</th> <th></th><th>Discount (%)</th>
									<th>Total (Hors taxes)</th> <th>&nbsp;</th><th>Taxe %</th> -->
								</tr>
								<tr>
									<td><input class="texte1" type="text" id="txtQte" name="txtQte" value="1"
												onkeyup="javascript: modif();" onfocus="javascript: this.select();">
									</td>
									<td> <input class="texte1_off" type="text" name="txtStockPtVente" id="txtStockPtVente" value="" disabled=""></td>
									<!-- Affichage du stock au depot de Rades pour l'article courant -->
									<td> <input class="texte1_off" type="text" name="txtStock" id="txtStock" value="" disabled=""></td>
								</tr>
							</tbody></table>
							<input class="button bouton_ajout_article" type="submit" id="sbmtEnvoyer" value="Add this article">
						</form>
			</div>
	</div>
</div>
<!-- Caddy display -->
<div class="col-lg-6">
		<div class="panel panel-default">
			<div class="panel-heading">Commande</div><br>
				<div class="panel-body">
									<div class="table-responsive">
                                        <table class="table table-bordered table-hover table-striped">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Désignation</th>
                                                    <th>Quantité</th>
                                                    <th>P.U</th>
                                                    <th>Total</th>                                                    
                                                    <th>Supprimer?</th>
                                                </tr>
                                            </thead>
                                            <tbody>
											    <?php
												$total_ht = 0;
												foreach($details as $key => $row) {
													$line_total = $row['quantity'] * $row['current_price'];
													echo "<tr> <td>".$row['sd_id']."</td>
															 <td>".$row['ref']."</td>
															 <td><input type='text' id='quantity_".$row['sd_id']."' value='".$row['quantity']."'      onblur=\"javascript: updatedQuantity(".$row['sd_id'].");\"></td>
															 <td><input type='text' id='price_".$row['sd_id']."'    value='".$row['current_price']."' onblur=\"javascript: updatedPrice(".$row['sd_id'].");\" ></td>
															 <td><input type='text' disabled id='total_".$row['sd_id']."' value='".$line_total."'></td>
															 <td><a href=\"\"><span class=\"glyphicon glyphicon-remove\"> </span></a></td> 
														 </tr>";
														 $total_ht += $line_total;
												}
											?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
				</div>
			</div>
</div>

</div>
<div class="row">
<!-- Paiement details -->
<div class="col-lg-12">
	<div class="panel panel-default">
			<div class="panel-heading">Paiement</div>
			<?php
				$percentage  = 18;
				$tax         = 0; // ($percentage / 100) * $total_ht;
				$total_ttc   = $total_ht + $tax;
			?>
				<div class="panel-body">
								<form id="frmDifference" class="formulaire1" method="post" 
								onSubmit="javascript: return verifReglement();" action="doCreateVenteGros">
					<button class="btn btn-success btn-lg btn-block" type="submit">Valider la commande</button>
					<input type="hidden" name="hdnChoix" value="">
									<input type="hidden" name="token" value="3fc894aba2cd321f33db13cff528838e">
										<h2>Montant</h2>
									<div class="panel-body">
										<div class="table-responsive"> 
										
											<table class="table table-bordered table-hover table-striped">
											
											<tr><th class="label1">Total TTC </th><th class="label1">Montant Reçu</th><th class="label1">Montant Reçu En Excés</th></tr>
											<tr>
											<!-- Affichage du montant du -->
											<td><input class="texte2_off" type="text" name="txtDu" id="txtDu"  value="<?php echo $total_ttc; ?>"></td>
											    <input type="hidden" id="computed_total_ht"  name="computed_total_ht" value="<?php echo $total_ht; ?>">
											    <input type="hidden" name="id_sale" value="<?php echo $id_sale; ?>">
											<!-- Choix du montant encaisse -->
											<td><input class="texte2" type="text" id="txtEncaisse" required name="txtEncaisse" value=""  onfocus="javascript: this.select();">
											</td>
											<!-- Affichage du montant rendu -->
											<td><input class="texte2_off" type="text" name="txtRendu" value="0" disabled=""></td>
											</tr>
											<tr>
											</tr></tbody></table>
										</div>
									</div>

								<HR>
									<h2>Type de payment </h2>
										<div class="form-group">
											<label class="radio">
												<input name="optionsRadios" id="optionsRadios_cash" value="100" checked="" type="radio">Cash
											</label>
											<label class="radio">
												<input name="optionsRadios" id="optionsRadios_carte" value="200" type="radio">Carte de Crédit
											</label>
											<label class="radio">
												<input name="optionsRadios" id="optionsRadios_cheque" value="300" type="radio">Chéque
											</label>
										</div>
								<HR>
									<p class="note_label">Notes<br><textarea class="form-control" name="txtaNotes"></textarea></p>
								</form>
				</div>


	</div>
</div>
</div>










</div>






</div>
</div><!-- #end div #main .wrapper -->
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/jquery/dist/jquery.min.js"></script>
		<link type="text/css" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.8.20.custom.css" rel="Stylesheet" />
		<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/jquery-ui.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/metisMenu/dist/metisMenu.min.js"></script>
    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/dist/js/sb-admin-2.js"></script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#example').DataTable({
                responsive: true,
				 "order": [[ 1, "desc" ]]
        });
    });
    </script>
</html>