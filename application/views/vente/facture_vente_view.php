<?php $this->load->view('template/header_beta_view.php'); ?>
<style>
@media print {
    #with_print {
        display: none;
    }
	#wrapper{
	   position:absolute; top:0px; left:0px; width:300px; z-index:1; 
	}
}
</style>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
			<?php 
				$this->load->view('template/topbar');
				$this->load->view('template/sidebar');
			?>
        </nav>
<!-- main content area -->   


<div id="page-wrapper">
<div class="row"> 
				<?php 
					echo '<br>';
					echo '<a id="with_print" href="'.base_url().'vente/vente/" class="btn btn-warning"> Retour au tableau de bord de vente</a>';
					echo '<br>';
				?>
<br><div class="col-lg-12">
<div class="panel panel-default">
		<div id="with_print" class="panel-heading">Récap vente</div><br>

 <div class="panel-body">

		<div class="inline-block" style="vertical-align: top"><div class="principal">
			<table id="with_print" class="table_resume">
				<tbody><tr><td class="resume_label">Facture</td><td><?php echo $facnumber;?></td></tr>
				<!--
				<tr><td class="resume_label">Total (Hors taxes)</td><td><?php // echo $total_ht;?> TND</td></tr>
				<tr><td class="resume_label">Tva</td><td>18%</td></tr>	
				-->

				<tr><td class="resume_label">Total (TTC) </td><td><?php echo $total_ttc; ?> TND</td></tr>
				<tr><td class="resume_label">Type de paiment </td><td>	Cash		</td></tr>
				<tr><td class="resume_label">Reçu</td><td><?php echo $encaisse;?> TND</td></tr><tr><td class="resume_label">Reçu en excés</td><td><?php 
				$arendre = $encaisse - $total_ttc;
				echo $arendre; ?> TND</td></tr>
				</tbody></table>
			<?php
			//var_dump($details);
			echo "<table  border='0'><thead><tr><td class='resume_label'>Référence</td><td>Quantité</td></tr><tbody>";
			foreach($details as $key => $detail) {
				echo "<tr><td class='resume_label'>".$detail['ref']." </td>";
				echo "<td class='resume_label'>".$detail['quantity']." </td></tr>";
			}
			echo "</tbody></table>";
			echo "<HR>";
			echo "Total TTC: ".$total_ttc. " TND";
			echo "<HR>";
			echo "Merci de votre visite";

			?>

				<div class="center"id="with_print">
					<HR>
					<button  type="button" onclick="window.print()" class="btn btn-outline btn-success btn-lg btn-block">Imprimer le ticket de caisse</button>
				</div>
		</div></div>

</div>
</div>
</div>
</div>
</div><!-- #end div #main .wrapper -->
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/jquery/dist/jquery.min.js"></script>
		<link type="text/css" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.8.20.custom.css" rel="Stylesheet" />
		<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/jquery-ui.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/metisMenu/dist/metisMenu.min.js"></script>
    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/dist/js/sb-admin-2.js"></script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#example').DataTable({
                responsive: true,
				 "order": [[ 1, "desc" ]]
        });
    });
    </script>
</html>