<?php $this->load->view('template/header_beta_view.php');?>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
			<?php 
				$this->load->view('template/topbar');
				$this->load->view('template/sidebar');
			?>
        </nav>
<!-- main content area -->   
<!-- main content area -->
<div id="page-wrapper">
<div class="row"><br>
<div class="row">
<!-- Paiement details -->
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">Scanner les articles </div>
			<div class="panel-body">
				<form id="frmDifference" class="formulaire1" method="post"  action="doCreateVenteDetails">
				 <!-- onkeyup="javascript: return verifScan(this); "  --> 
									<p class="note_label">Articles<br><textarea rows="5"
									class="form-control" id="scannedArticles" name="scannedArticles"></textarea></p>
								<HR>
					<button class="btn btn-success btn-lg btn-block" type="submit">Mettre à jour la commande</button>
				</form>
</div> </div> </div>
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">Commande</div>
			<div class="panel-body">
						<div class="table-responsive">
                                   <table class="table table-bordered table-hover table-striped">
                                            <thead>
                                                <tr>
                                                    <th>#</th><th>Reference</th><th>Code à barre</th><th>P.U (Tunis)</th><th>P.U (H-lif)</th><th>Stock disponible</th>
													<th>Supprimer?</th>
                                                </tr>
                                            </thead>
                                            <tbody>
											    <?php
												if(!empty($details)){
													foreach($details as $key => $row) {
														echo "<tr>
														<td>". $key."</td>
														<td>".$row['reference']."</td>
														<td>".$row['codeBarre']."</td>
														<td>".$row['prixTunis']."</td><td>".$row['prixHlif']."</td><td>".$row['stockPcs']."</td><td> X </td></tr>";
													}
								
												} else {
													echo '   <tr><td colspan="4">Aucun article pour le moment</td></tr>';
												}
												?>
						</div>
 </div> </div> </div>



<?php
		if(!empty($details)){
					echo '<br>
					        <form id="frmDifference" class="formulaire1" method="post" onsubmit="javascript: return verifBarcodes()" action="validateVenteDetails">
								<input type="hidden" id="sales_id"  name="sales_id" value="'.$id_sale.'" >
								<input type="hidden" id="previously_scanned_articles"  name="previously_scanned_articles" value="'.$details.'" >
								
														<h2>Montant</h2>
									<div class="panel-body">
										<div class="table-responsive"> 
										
											<table class="table table-bordered table-hover table-striped">
											
											<tr><th class="label1">Total TTC </th><th class="label1">Montant Reçu</th><th class="label1">Montant Reçu En Excés</th></tr>
											<tr>
											<!-- Affichage du montant du -->
											<td><input class="texte2_off" type="text" name="txtDu" id="txtDu"  value="'.$total_ttc.'"></td>
											    <input type="hidden" id="computed_total_ht"  name="computed_total_ht" value="'.$total_ht.'">
											    <input type="hidden" name="id_sale" value="'.$id_sale.'">
											<!-- Choix du montant encaisse -->
											<td><input class="texte2" type="text" id="txtEncaisse" required name="txtEncaisse" value=""  onfocus="javascript: this.select();">
											</td>
											<!-- Affichage du montant rendu -->
											<td><input class="texte2_off" type="text" name="txtRendu" value="0" disabled=""></td>
											</tr>
											<tr>
											</tr></tbody></table>
										</div>
									</div>
								
								<button class="btn btn-danger btn-lg btn-block" type="submit">Valider la commande</button>
							</form>';
        }
?>
</div>
</div>
</div><!-- #end div #main .wrapper -->
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/jquery/dist/jquery.min.js"></script>
		<link type="text/css" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.8.20.custom.css" rel="Stylesheet" />
		<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/jquery-ui.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/metisMenu/dist/metisMenu.min.js"></script>
    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/dist/js/sb-admin-2.js"></script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
	$('#scannedArticles').keypress(function(e){
			if (e.keyCode == 32 ||e.keyCode == 0) {
				$('#scannedArticles').val($('#scannedArticles').val()+',');
		}
	});
    $(document).ready(function() {
        $('#example').DataTable({
                responsive: true,
				 "order": [[ 1, "desc" ]] });
    });
    </script>
</html>