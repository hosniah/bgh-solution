<?php $this->load->view('template/header_beta_view.php'); ?>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
			<?php 
				$this->load->view('template/topbar');
				$this->load->view('template/sidebar');
			?>
        </nav>
        <div id="page-wrapper">
            <div class="row">
<!-- main content area -->   
<div class="wrapper" id="main"> 

	<!-- content area -->    
	<section id="content" class="wide-content">
	<div class="wrapper">
		<h1><?php echo 'Liste des commandes'; ?> 
	<?php
			if (isset($generate)) {
				echo '<br>' . $generate['start'] . ' - ' . $generate['end'];
			}
			if (isset($user)) {
				echo ': ' . $user['login'];
			}
			?></h1>
    </div>

<div class="panel-body">
     <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
								       <thead>
				<tr><th><?php echo 'date'; ?></th>
					<th><?php echo 'client'; ?></th>
                                        <th><?php echo 'point de vente'; ?></th>
					<th><?php echo 'Status'; ?></th>
					<th><?php echo 'Action'; ?></th></tr>
					       </thead> <tbody>
				<?php foreach ($orders as $order) { ?>
					<tr>
						<td><?php echo $order['date_commande']; ?></td>
						<td><?php echo $order['id_client']; ?></td>
                                                <td><?php echo $order['id_point_vente']; ?></td>
						<td><?php echo $order['status']; ?></td>
						<td>
						<?php echo '<a href="'.base_url().'stock/commande/details/'.$order['id'].'" class="btn btn-danger">Modifier</a>';
								if($order['status'] == 'Commande Validée') {
								
									echo '&nbsp;&nbsp;<a href="'.base_url().'warehouse/order/bonsortie/'.$order['id'].'" class="btn btn-success">Bon de sortie</a>';
								} else {
                                   echo '&nbsp;&nbsp;<a href="'.base_url().'stock/commande/depot_valider_commande/'.$order['id'].'" class="btn btn-warning">Valider</a>';
								}
						?>
						</td>
					</tr>
				<?php } ?>
				 </tbody>
			</table>
	</section><!-- #end content area -->
	
</div></div>
</div><!-- #end div #main .wrapper -->
</div>
</div>
</div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/jquery/dist/jquery.min.js"></script>
		<link type="text/css" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.8.20.custom.css" rel="Stylesheet" />
		<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/jquery-ui.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/metisMenu/dist/metisMenu.min.js"></script>
    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/dist/js/sb-admin-2.js"></script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true,
				 "order": [[ 1, "desc" ]]
        });
    });
    </script>

</html>
<?php //$this->load->view('template/footer_view.php'); ?>