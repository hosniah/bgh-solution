<?php $this->load->view('template/header_beta_view.php'); ?>

<body>
<script src="<?php echo base_url();?>js/featherlight/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
<link href="<?php echo base_url();?>js/featherlight/release/featherlight.min.css" type="text/css" rel="stylesheet" />
<div id="wrapper">
<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
<?php
    $this->load->view('template/topbar');
    $this->load->view('template/sidebar');
?>
</nav>
<div id="page-wrapper">
<div class="row">
<!-- main content area -->  
<!-- main content area -->
<div class="col-lg-12">
<div class="panel-body">
<div class="dataTable_wrapper">
<?php if (isset($error)): ?>
    <div class="alert alert-error"><?php echo $error; ?></div>
<?php endif; ?>
<?php if ($this->session->flashdata('success') == TRUE): ?>
        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
<?php endif; ?>
<section id="page-header" class="clearfix">    
	<div class="wrapper">
		<?php if (isset($user['name'])) { ?> 
			<h1><?php echo $this->lang->line('edit'); ?>: <?php echo $user['name']; ?></h1>
		<?php } else { ?>
			<h1><?php echo $this->lang->line('h1_add_packing'); ?></h1>
		<?php } ?>
    </div>

</section>


<!-- main content area -->   
<div class="wrapper" id="main"> 

	<!-- content area -->    
	<section id="content" class="wide-content">
		<div class="grid_4">
			<form action="" method="POST">				
				<input type="hidden" name="sent" value="yes">
				<div class="label"><?php echo $this->lang->line('name'); ?></div>
				<input type="text" name="name" value="<?php
				if (isset($user['name'])) {
					echo $user['name'];
				}
				?>" required>
				<div class="label"><?php echo $this->lang->line('desc'); ?></div>
				<input type="text" name="desc" value="<?php
				if (isset($user['desc'])) {
					echo $user['desc'];
				}
				?>" required>
				<input type="submit" value="<?php echo $this->lang->line('submit'); ?>">
			</form>
		</div>
		<div class="grid_8">
			<table>
				<tr><!--<th><?php //echo $this->lang->line('id');    ?></th>-->
					<th><?php echo $this->lang->line('name'); ?></th>
					<th><?php echo $this->lang->line('desc'); ?></th>
					<th><?php echo $this->lang->line('actions'); ?></th></tr>
				<?php foreach ($products as $product) { ?>
					<tr>
						<!--<td><?php //echo $product['id'];    ?></td>-->
						<td><?php echo $product['name']; ?></td>
						<td><?php echo $product['desc']; ?></td>
						<td>
							<?php if ($this->session->userdata('admin_lvl')) { ?>
								<?php if ($product['null'] != 0) { ?>
									<a href="<?php echo base_url(); ?>warehouse/packing/del/<?php echo $product['id']; ?>" onclick="return confirm(<?php echo $this->lang->line('confirm_delete'); ?>);"><?php echo $this->lang->line('del'); ?></a>
								<?php } else { ?>
									<?php echo $this->lang->line('del'); ?>
								<?php } ?>
							<?php } ?>
							<a href="<?php echo base_url(); ?>warehouse/packing/edit/<?php echo $product['id']; ?>"><?php echo $this->lang->line('edit'); ?></a>
						</td>
					</tr>
				<?php } ?>			
			</table>
		</div>
	</section><!-- #end content area -->

</div><!-- #end div #main .wrapper -->

</div>
</div>
</div>


</div></div>

</div><!-- #end div #main .wrapper -->
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/jquery/dist/jquery.min.js"></script>
<link type="text/css" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.8.20.custom.css" rel="Stylesheet" />
<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/jquery-ui.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
$(document).ready(function() {
    $('#example').DataTable({
        responsive: true,
        "order": [[ 1, "desc" ]]
    });
});
</script>
</html>
<?php // $this->load->view('template/footer_view_datatables.php'); ?>