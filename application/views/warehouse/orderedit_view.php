<?php $this->load->view('template/header_view.php'); ?>

<section id="page-header" class="clearfix">    
	<div class="wrapper">
		<?php if (isset($user['name'])) { ?> 
			<h1><?php echo $this->lang->line('edit'); ?>: <?php echo $user['name']; ?></h1>
		<?php } else { ?>
			<h1><?php echo 'Ajout d\'une commande'; ?></h1>
		<?php } ?>
    </div>

</section>


<!-- main content area -->   
<div class="wrapper" id="main"> 

	<!-- content area -->    
	<section id="content" class="wide-content">
		<div class="grid_4">
			<form action="" method="POST">
				<input type="hidden" name="sent" value="yes">
				<div class="label"><?php echo 'Reference'; ?></div>
				<input type="text" name="name" value="<?php
				if (isset($user['reference'])) {
					echo $user['reference'];
				}
				?>" required>
				<div class="label"><?php echo 'Quantité'; ?></div>
				<input type="text" name="quantite" value="<?php
				if (isset($user['quantite'])) {
					echo $user['quantite'];
				}
				?>" required>
				<input type="submit" value="<?php echo 'Créer la commande'; ?>">
			</form>
		</div>
		<div class="grid_8">
			<table>
				<tr><th><?php echo 'Commande #';    ?></th>
					<th><?php echo 'Reference'; ?></th>
					<th><?php echo 'Quantité';  ?></th>
					<th><?php echo $this->lang->line('actions'); ?></th></tr>
				<?php foreach ($orders as $order) { ?>
					<tr>
						<td><?php echo $order['id'];    ?></td>
						<td><?php echo $order['reference']; ?></td>
						<td><?php echo $order['quantite']; ?></td>
						<td>
							<?php if ($this->session->userdata('admin_lvl')) { ?>
								<a href="<?php echo base_url(); ?>warehouse/order/del/<?php echo $order['id']; ?>" onclick="return confirm(<?php echo $this->lang->line('confirm_delete'); ?>);"><?php echo $this->lang->line('del'); ?></a>
							<?php } ?>
							<a href="<?php echo base_url(); ?>warehouse/order/edit/<?php echo $order['id']; ?>"><?php echo $this->lang->line('edit'); ?></a>
						</td>
					</tr>
				<?php 
				}
			?>
			</table>
		</div>
	</section><!-- #end content area -->
</div><!-- #end div #main .wrapper -->
<?php $this->load->view('template/footer_view.php'); ?>