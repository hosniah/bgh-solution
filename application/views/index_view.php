<?php $this->load->view('template/header_view.php'); ?>

<!-- hero area (the grey one with a slider -->
<section id="hero" class="clearfix">    
    <!-- responsive FlexSlider image slideshow -->
    <div class="wrapper">
		<div class="row"> 

			<div class="grid_7 rightfloat">
				<div class="flexslider">
					<ul class="slides">
						<li>
							<img src="<?php echo base_url(); ?>images/warehouse-pic3.jpg" />
							<p class="flex-caption">Easy to view on mobile phones and tablets</p>
						</li>
						<li>
							<img src="<?php echo base_url(); ?>images/warehouse-pic4.jpg" />
							<p class="flex-caption">System log, so nothing can hide.</p>
						</li>
						<li>
							<img src="<?php echo base_url(); ?>images/warehouse-pic1.jpg" />
							<p class="flex-caption">One click backup.</p>
						</li>
						<li>
							<img src="<?php echo base_url(); ?>images/warehouse-pic2.jpg" />
							<p class="flex-caption">Easy to export to CSV (and view in Microsoft Excel or Calc).</p>
						</li>
					</ul>
                </div><!-- FlexSlider -->
			</div><!-- end grid_7 -->
        </div><!-- end row -->
	</div><!-- end wrapper -->
</section><!-- end hero area -->

<!-- main content area -->   
<div class="wrapper" id="main"> 

	<!-- content area -->    
	<section id="content" class="wide-content">
		<h1>Dibbou manager cover</h1>

	</section><!-- #end content area -->
</div>

<section id="buy" class="blueelement vertical-padding">

</section>
<div class="wrapper" id="demo"> 

	<!-- content area -->    

</div><!-- #end div #main .wrapper -->

<?php $this->load->view('template/footer_view.php'); ?>