<?php $this->load->view('template/header_beta_view.php'); ?>

<body>
<script src="<?php echo base_url();?>js/featherlight/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
<link href="<?php echo base_url();?>js/featherlight/release/featherlight.min.css" type="text/css" rel="stylesheet" />
<div id="wrapper">
<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
<?php
    $this->load->view('template/topbar');
    $this->load->view('template/sidebar');
?>
</nav>
<div id="page-wrapper">
<div class="row">
<!-- main content area -->  
<!-- main content area -->
<div class="col-lg-12">
<div class="panel-body">
<div class="dataTable_wrapper">
<?php if (isset($error)): ?>
    <div class="alert alert-error"><?php echo $error; ?></div>
<?php endif; ?>
<?php if ($this->session->flashdata('success') == TRUE): ?>
        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
<?php endif; ?>
<h2>Hisotrique des commandes:</h2>
<HR>

<table id="example" class="" cellspacing="0" width="90%">
<thead>
<tr><th>#</th><th>Magasin</th><th>Date</th><th>Etat</th></tr>
</thead>
<tbody>
<form method="POST" action="" enctype="multipart/form-data"> 
<?php

if (empty($historique)){
    echo '<tr><td colspan="4">Aucune commande pour le moment</td></tr>';
} else {
    foreach ($historique as $row) {
		echo'<tr>
			<td>'.$row['id'].'</td>
			<td>'.$row['id_client'].'</td>
			<td>'.$row['date_commande'].'</td>
			<td>'.$row['status'].'</td>
		</tr>';
    }
}
?>
</form>    
</tbody>
</table>
</div>
</div>
</div>
</div>


</div></div>

</div><!-- #end div #main .wrapper -->
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/jquery/dist/jquery.min.js"></script>
<link type="text/css" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.8.20.custom.css" rel="Stylesheet" />
<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/jquery-ui.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
$(document).ready(function() {
    $('#example').DataTable({
        responsive: true,
        "order": [[ 1, "desc" ]]
    });
});
</script>
</html>
<?php // $this->load->view('template/footer_view_datatables.php'); ?>