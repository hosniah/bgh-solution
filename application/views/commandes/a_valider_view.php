<?php $this->load->view('template/header_beta_view.php'); ?>
<body>
<script src="<?php echo base_url();?>js/featherlight/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
<link href="<?php echo base_url();?>js/featherlight/release/featherlight.min.css" type="text/css" rel="stylesheet" />
<div id="wrapper">
<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
<?php
    $this->load->view('template/topbar');
    $this->load->view('template/sidebar');
?>
</nav>
<div id="page-wrapper">
<div class="row">
<!-- main content area -->  
<div class="col-lg-12">
<div class="panel-body">
<div class="dataTable_wrapper">
<?php if (isset($error)): ?>
    <div class="alert alert-error"><?php echo $error; ?></div>
<?php endif; ?>
<?php if ($this->session->flashdata('success') == TRUE): ?>
        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
<?php endif; ?>
<h2> Listes des commandes livrés par le dépôt, en attente de validation: </h2>
<HR>
<?php
if (empty($avalider)) {
    echo '<span>Aucune commande pour le moment</span>';
} else {
    echo '
		<table id="example" class="" cellspacing="0" width="90%">
		<thead>
		<tr><th>Numéro</th><th>Date</th><th>Description</th><th>Etat</th><th>Action</th></tr>
		</thead>
		<tbody>
		';
    foreach ($avalider as $row) { 
 //      var_dump($avalider);
        echo'<tr>
                <td>'. $row['id'].'</td>
                <td>'. $row['date_commande'].'</td>
                <td>'. $row['description'].'</td>
                <td>'. $row['status'].'</td>
                <input type="hidden" name="id" value='.$row['id'].' />
                <td><button class="btn btn-danger" data-toggle="modal" data-target="#myModal">valider la réception de la commande       </button>
				<!-- <button class="btn btn-danger"  type="submit" id="sbmtEnvoyer" value="">valider la réception de la commande</button>-->';
				echo '  <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Validation de la réception de la commande N°: '. $row['id'].'</h4>
                                        </div>
                                        <div class="modal-body">

<form method="POST" action="'. base_url().'commandes/commandes/validationPointDeVente" enctype="multipart/form-data">
	<input type="hidden" id="id" name="id" value="'. $row['id'].'">
        <button type="submit" class="btn btn-lg btn-success">Valider Toute la commande</button>
</form>
<HR>
<form method="POST" action="'.base_url().'commandes/commandes/validationPointDeVente" enctype="multipart/form-data">
				<div class="row">
					<label>Liste des articles commandés:</label>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th><th>Reference</th><th>Quantité</th><th>Commentaire</th><th>Validé?</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
						foreach($row['order_details'] as $product){
                                        echo "<tr>
                                            <td>".$product->{'id'}."</td><td>".$product->{'reference'}."</td><td>".$product->{'quantite'}."</td><td>".$product->{'commentaires'}."</td>
                                            <td><input value=\"\" type=\"checkbox\"></td></tr>";
                                    }
                                    echo ' </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                                </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                            <button type="button" class="btn btn-lg btn-danger">Valider uniquement les articles cochés</button>
                                        </div>
							</form>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
				</td>
        </tr>';
    }
	echo '
		</tbody>
	</table>';
}
?>
</div>
</div>
</div>
</div>


</div></div>

</div><!-- #end div #main .wrapper -->
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/jquery/dist/jquery.min.js"></script>
<link type="text/css" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.8.20.custom.css" rel="Stylesheet" />
<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/jquery-ui.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
$(document).ready(function() {
    $('#example').DataTable({
        responsive: true,
        "order": [[ 1, "desc" ]]
    });
});
</script>
</html>
<?php // $this->load->view('template/footer_view_datatables.php'); ?>
