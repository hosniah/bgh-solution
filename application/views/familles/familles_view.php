<?php $this->load->view('template/header_beta_view.php'); ?>

<body>
<script src="<?php echo base_url();?>js/featherlight/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
<link href="<?php echo base_url();?>js/featherlight/release/featherlight.min.css" type="text/css" rel="stylesheet" />
<div id="wrapper">
<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
<?php
    $this->load->view('template/topbar');
    $this->load->view('template/sidebar');
?>
</nav>
<div id="page-wrapper">
<div class="row">
<!-- main content area -->  
<!-- main content area -->
<div class="col-lg-12">
<div class="panel-body">
<div class="dataTable_wrapper">
<?php if (isset($error)): ?>
    <div class="alert alert-error"><?php echo $error; ?></div>
<?php endif; ?>
<?php if ($this->session->flashdata('success') == TRUE): ?>
        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
<?php endif; ?>
<section id="page-header" class="clearfix">    
	<div class="wrapper">
		<h1><?php echo $this->lang->line('h1_notes_manage'); ?></h1>
    </div>

</section>


<!-- main content area -->   
<div class="wrapper" id="main"> 

	<!-- content area -->    
	<section id="content" class="wide-content">
		<a href="<?php echo base_url(); ?>familles/famille/edit/0"><?php echo $this->lang->line('new'); ?></a>
		<table>
			<tr>
                        <th><?php echo $this->lang->line('h1_id_famille'); ?></th>
                        <th><?php echo $this->lang->line('h1_label_famille'); ?></th>
                        <th><?php echo $this->lang->line('h1_code_famille'); ?></th>
                        <!--<th><?php //echo $this->lang->line('h1_status_famille'); ?></th>-->
                        <th colspan="2"><?php echo $this->lang->line('h1_action_famille'); ?></th>
			<?php $i = 1; ?>
			<?php foreach ($familles as $famille) { ?>
				<tr>
					<td><?php echo $famille['id_famille']; ?></td>
					<td><?php echo $famille['label_famille']; ?></td>
                                        <td><?php echo $famille['code_famille']; ?></td>		
					<td>
						<a href="<?php echo base_url(); ?>familles/famille/del/<?php echo $famille['id_famille']; ?>" onclick="return confirm(<?php echo $this->lang->line('confirm_delete'); ?>);"><?php echo $this->lang->line('del'); ?></a>
						<a href="<?php echo base_url(); ?>familles/famille/edit/<?php echo $famille['id_famille']; ?>"><?php echo $this->lang->line('edit'); ?></a>
					</td>
				</tr>
			<?php } ?>			
		</table>
	</section><!-- #end content area -->

</div><!-- #end div #main .wrapper -->


</div>
</div>
</div>
</div>


</div></div>

</div><!-- #end div #main .wrapper -->
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/jquery/dist/jquery.min.js"></script>
<link type="text/css" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.8.20.custom.css" rel="Stylesheet" />
<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/jquery-ui.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
$(document).ready(function() {
    $('#example').DataTable({
        responsive: true,
        "order": [[ 1, "desc" ]]
    });
});
</script>
</html>
<?php // $this->load->view('template/footer_view_datatables.php'); ?>