<?php $this->load->view('template/header_beta_view.php'); ?>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
  <?php
echo ' <script>
  $(function() {
    var availableTags = '.$products_list.';
    $( "#id" ).autocomplete({
      source: availableTags
    });
  });
</script>
'?>
 
 
 
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
			<?php 
				$this->load->view('template/topbar');
				$this->load->view('template/sidebar');
			?>
        </nav>
        <div id="page-wrapper">
<!-- main content area -->   
<div class="wrapper" id="main"> 

<section id="page-header" class="clearfix">    
	<div class="wrapper">
		<h1><?php echo 'Création de commande'; ?> <?php
			if (isset($generate)) {
				echo '<br>' . $generate['start'] . ' - ' . $generate['end'];
			}
			if (isset($user)) {
				echo ': ' . $user['login'];
			}
			?></h1>
    </div>

</section>

<!-- main content area -->   
<div class="wrapper" id="main"> 
	<!-- content area -->    
	<section id="content" class="wide-content">
		<div class="grid_4">
			<form action="" method="POST">
				<input type="hidden" name="sent" value="yes">
                Reference: <input class="texte_ref" type="text" id="id" name="id" value=""> <br> <br>

			<?php echo $this->lang->line('order'); ?>
				<?php foreach ($orders as $order) { ?>
										<?php
					if (isset($order['id'])) {
						//echo $order['id'];
					}
					?>">
					<?php
					if (isset($order['name'])) {
						//echo $order['name'];
					}
					?>
				<?php } ?>
				<?php echo "Qauntité"; ?>
				<input type="number" name="amount">
				<input type="submit" value="<?php echo 'Ajouter à la commande'; ?>">
			</form>
		</div>
		<div class="grid_8">
			<table>
				<?php
				if (!empty($reports)) {
					echo "<tr>
                                                <th>Image</th>
						<th>". $this->lang->line('product')."</th>
						<th>". $this->lang->line('amount')."</th>
						<th> Stock disponible en pcs</th>
						<th> Actions ? </th>
						</tr>";
				}


				foreach ($reports as $report) {

                        $rades = $this->library_model->getProductStock($report['reference']);

					$sum = 0;
					$id = $report['id'];
					//var_dump($products[$id]['stockPcs']);
					$notAvailableFlag = '';
				//	if ($products[$id]['stockPcs'] < $report['sum']) {
                                       if ($report['stock'] <  $report['sum']){
						$notAvailableFlag = 'style="color: #fff; background: red;"';
					}
					echo '<tr '.$notAvailableFlag.'>';
					?>
						<td><img height="60" width="60" src="<?php echo base_url(); ?>images/products/<?php echo $report['reference']; ?>.jpg" /></td>
                                                <td><?php echo $report['reference']; ?></td>       <td><?php echo $report['sum']; ?></td>
						<td><?php echo $rades[0]->{'stock_total'}; ?></td>
                                                <td><a href="<?php echo base_url(); ?>warehouse/order/delTempOrders/<?php echo $report['id_temp_orders']; ?>" class="btn btn-warning" > Supprimer </a></td>
					</tr>
				<?php } ?>
			</table><br><br>
			<?php
			if (!empty($reports)) {
				echo '
				<form action="'.base_url().'warehouse/order/createOrder" method="POST">';
				if ($userID = $this->session->userdata('user_id')) {
					echo ' <input type="hidden" name="user_id" value="'.$userID.'"> <input type="submit"  value="Créer la commande"><br><br>';
				}
				echo '</form>';
				echo '<a href="'.base_url().'warehouse/order/delAllTempOrders"><input type="button" class="btn btn-danger"  name="delete" value="Annuler la commande"></a>';
			} else {
				echo "<b>Vous devez selectionner un article et saisir une quantité depuis le formulaire à gauche !</b><br>";
			}
			?>
		</div>
	</section><!-- #end content area -->
</div><!-- #end div #main .wrapper -->
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/jquery/dist/jquery.min.js"></script>
		<link type="text/css" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.8.20.custom.css" rel="Stylesheet" />
		<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/jquery-ui.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/metisMenu/dist/metisMenu.min.js"></script>
    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/dist/js/sb-admin-2.js"></script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#example').DataTable({
                responsive: true,
				 "order": [[ 1, "desc" ]]
        });
    });
    </script>
</html>
