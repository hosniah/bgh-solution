<div class="navbar-default sidebar" role="navigation">
<div class="sidebar-nav navbar-collapse">
<ul class="nav" id="side-menu">
<li class="sidebar-search">
<div class="input-group custom-search-form">
<img width="200px" height="180px" src="<?php echo base_url(); ?>images/logo.jpg" ></img>
</div>
<!-- /input-group -->
</li>
<?php 
if (($this->session->userdata('admin_lvl')) ||
($this->session->userdata('superadmin_lvl'))) 
{
?>
<li>
<a href="#">
<i class="fa fa-dashboard  fa-fw"></i> <?php  echo 'Produits'; // echo $this->lang->line('menu_production'); ?>
<span class="fa arrow"></span>
</a>
<ul class="nav nav-second-level">
<li><a href="<?php echo base_url(); ?>warehouse/listProducts">Tableau de board</a></li>
<li><a href="<?php echo base_url(); ?>warehouse/add/import">Import de produits</a></li>
<!--   <li><a href="<?php echo base_url(); ?>warehouse/add/production"><?php echo "Correction de stock";//echo $this->lang->line('menu_inproduction'); ?></a></li> -->
<!-- <li><a href="<?php echo base_url(); ?>enc/proreports"><?php echo $this->lang->line('menu_proreport'); ?></a></li> -->
</ul>
</li>
<?php
}
echo '<li><a href="#"><i class="fa fa-wrench fa-fw"></i>Commandes<span class="fa arrow"></span></a>';
echo '<ul class="nav nav-second-level">';
if(($this->session->userdata('responsabledepot_lvl')) || ($this->session->userdata('admin_lvl')) || ($this->session->userdata('superadmin_lvl'))) {
    echo '<li><a href="'. base_url().'warehouse/order">Lister les commandes</a></li>
<li><a href="'. base_url().'warehouse/order/waitingForValidation">Validation</a></li>';
} 
if (($this->session->userdata('admin_lvl')) ||
($this->session->userdata('supervendeur_lvl')) ||
($this->session->userdata('gerant_lvl'))) { 
?>
<li><a href="<?php echo base_url(); ?>warehouse/order/add">Nouvelle commande</a></li>
<li><a href="<?php echo base_url(); ?>commandes/commandes/encours">Commande en cours</a></li>
<li><a href="<?php echo base_url(); ?>commandes/commandes/avalider">Commande a valider</a></li>
<li><a href="<?php echo base_url(); ?>commandes/commandes/historique">Historique des commandes</a></li>
                        <!-- <li><a href="<?php //echo base_url(); ?>warehouse/order/add">Nouvelle commande</a></li> -->
<?php }
echo ' </ul> </li>';
if (($this->session->userdata('admin_lvl')) ||
($this->session->userdata('superadmin_lvl'))) { ?>
<li><a href="#"><i class="fa  fa-bar-chart-o fa-fw"></i><?php echo $this->lang->line('menu_famille'); ?><span class="fa arrow"></span></a>
<ul class="nav nav-second-level">
<li><a href="<?php echo base_url(); ?>familles/famille"><?php echo $this->lang->line('menu_list_famille'); ?></a></li>
<li><a href="<?php echo base_url(); ?>familles/sousFamille"><?php echo $this->lang->line('menu_list_sous_famille'); ?></a></li>
</ul>
</li>
<?php } ?>
<?php if (($this->session->userdata('admin_lvl')) || ($this->session->userdata('superadmin_lvl'))) {									?>
<ul class="nav nav-second-level">
<li><a href="<?php echo base_url(); ?>stock/pointvente"><?php echo 'Points de vente'; ?></a></li>
</ul>
</li>
<?php
}
if (($this->session->userdata('admin_lvl')) ||
($this->session->userdata('supervendeur_lvl')) ||
($this->session->userdata('gerant_lvl')) ||
($this->session->userdata('superadmin_lvl')) ||
($this->session->userdata('responsabledepot_lvl'))) { ?>
<li><a href="#"> <i class="fa fa-files-o fa-fw"></i><?php echo 'Stock'; ?><span class="fa arrow"></span></a>
<ul class="nav nav-second-level">
<?php
if($this->session->userdata('responsabledepot_lvl')) {
?>
<li><a href="<?php echo base_url(); ?>warehouse/listProducts"><?php echo 'Stock dépot'; ?></a></li>
<?php
} else {
?>
<li><a href="<?php echo base_url(); ?>stock/stockpointvente"><?php echo 'Stock point de vente'; ?></a></li>
<?php
}
?>
</ul>
</li>
<?php 
}
if (($this->session->userdata('admin_lvl')) ||
($this->session->userdata('gerant_lvl')) ||
($this->session->userdata('superadmin_lvl'))) { ?>
<li><a href="#"><i class="fa fa-files-o fa-fw"></i><?php echo 'Vente'; ?><span class="fa arrow"></span></a>
<ul class="nav nav-second-level">
<li><a href="<?php echo base_url(); ?>vente/vente/list_all"> 
<?php echo 'Recap des ventes'; ?></a></li>
<?php
}
if (($this->session->userdata('user_lvl')) ||
($this->session->userdata('admin_lvl')) ||
($this->session->userdata('supervendeur_lvl')) ||
($this->session->userdata('gerant_lvl')) ||
($this->session->userdata('superadmin_lvl'))) {
?>
<li><a href="<?php echo base_url(); ?>vente/vente"> 
<?php echo 'Nouvelle Vente'; ?></a></li>
</ul>
</li>
<?php
}
if ($this->session->userdata('superadmin_lvl')) { ?>
            <li><a href="#"><i class="fa  fa-table fa-fw"></i><?php echo "Catalogue de produits"; ?><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
                <li><a href="<?php echo base_url(); ?>warehouse/library"><?php echo "Stock Rades"; ?></a></li>
                <li><a href="<?php echo base_url(); ?>warehouse/library"><?php echo "Stock des points de ventes"; ?></a></li>
                <li><a href="<?php echo base_url(); ?>warehouse/prices"><?php echo "Gestion des prix"; ?></a></li>
               <!-- <li><a href="<?php echo base_url(); ?>warehouse/packing"><?php echo $this->lang->line('menu_lib_packing'); ?></a></li> -->
<!-- <li><a href="<?php // echo base_url();     ?>warehouse/order"><?php // echo $this->lang->line('menu_lib_orders');     ?></a></li> -->
            </ul>
            </li>
<?php
}
if ($this->session->userdata('superadmin_lvl')) {
?>
<li><a href="#"><i class="fa fa-files-o fa-fw"></i><?php echo $this->lang->line('menu_users'); ?><span class="fa arrow"></span></a>
<ul class="nav nav-second-level">
    <li><a href="<?php echo base_url(); ?>users/manage"><?php echo "Gestion des utilisateurs";//echo $this->lang->line('menu_manage'); ?></a></li>
</ul>
</li>
<?php
}
?>
</ul>
</div>
<!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
