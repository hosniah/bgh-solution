<?php $this->load->view('template/header_beta_view.php'); ?>
<style>
@media print {
    #with_print {
        display: none;
    }
}
</style>

<body>
<script src="<?php echo base_url();?>js/featherlight/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
<link href="<?php echo base_url();?>js/featherlight/release/featherlight.min.css" type="text/css" rel="stylesheet" />
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
			<?php 
				$this->load->view('template/topbar');
			//	$this->load->view('template/sidebar');
			?>

			</nav>
        <div id="page-wrapper">
            <div class="row">
				<?php 
					echo '<br>';
					echo '<a id="with_print" href="'.base_url().'warehouse/order/" class="btn btn-warning"> Retour à la liste des commandes</a>';
					echo '<br>';
				?>
<!-- main content area -->  
<br><div class="col-lg-12">
<div class="panel panel-default">
		<div class="panel-heading">Bon de sortie</div><br>

 <div class="panel-body">
 	<button id="with_print" type="button" onclick="window.print()" class="btn btn-outline btn-success btn-lg btn-block">Imprimer</button>

                            <h3>Bon de sortie de la commande numèro: <?php echo $id; ?></h3>
                             <p id="with_print" >Ci-dessous la commande validée par l'agent du dépôt:</p>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    
                                    <tbody>
                                        <tr>
                                            <th>Date commande</th>
                                            <td><?php echo $date_commande; ?></td>
                                            <td colspan="3"><b>Point de vente:</b>  <?php echo $point_vente;?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="5"><b><center>Articles à livrer:</center></b></td>
                                        </tr>
										<tr>
                                            <th>Reference</th>
                                            <th>Quantité Pcs</th>
                                            <th>Colisage</th>
                                            <th>CTN</th>
                                        </tr>
										<?php
											foreach($order_details as $row) {
												echo "<tr>
													<td>".$row->{'reference'}."</td>
													<td> ".$row->{'quantite'}."<code>pcs</code></td>
													<td>".$row->{'colisage'}."</td>
													<td>".$row->{'ctn'}."</td>
												</tr>";
											}
										?>
										



                                    </tbody>
                                </table>
                            </div>
                          <!--  <p>Grid classes apply to devices with screen widths greater than or equal to the breakpoint sizes, and override grid classes targeted at smaller devices. Therefore, applying any
                                <code>.col-md-</code> class to an element will not only affect its styling on medium devices but also on large devices if a
                                <code>.col-lg-</code> class is not present.</p>
								-->
                        </div>

   
	
	
	
	

</div>
</div>

</div>


</div></div>

</div><!-- #end div #main .wrapper -->
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/jquery/dist/jquery.min.js"></script>
		<link type="text/css" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.8.20.custom.css" rel="Stylesheet" />
		<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/jquery-ui.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/metisMenu/dist/metisMenu.min.js"></script>
    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/dist/js/sb-admin-2.js"></script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#example').DataTable({
                responsive: true,
				 "order": [[ 1, "desc" ]]
        });
    });
    </script>
</html>
<?php // $this->load->view('template/footer_view_datatables.php'); ?>