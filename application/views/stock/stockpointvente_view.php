<?php $this->load->view('template/header_beta_view.php'); ?>

<body>
<script src="<?php echo base_url();?>js/featherlight/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
<link href="<?php echo base_url();?>js/featherlight/release/featherlight.min.css" type="text/css" rel="stylesheet" />
<div id="wrapper">
<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
<?php
    $this->load->view('template/topbar');
    $this->load->view('template/sidebar');
?>
</nav>
<div id="page-wrapper">
<div class="row">
<!-- main content area -->  
<!-- main content area -->
<div class="col-lg-12">
<div class="panel-body">
<div class="dataTable_wrapper">
<?php if (isset($error)): ?>
    <div class="alert alert-error"><?php echo $error; ?></div>
<?php endif; ?>
<?php if ($this->session->flashdata('success') == TRUE): ?>
        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
<?php endif; ?>
<h2>Tableau de boards des produits</h2>
<HR>

<div class="panel panel-default">
<div class="panel-heading">Recherche</div><br>  <div class="panel-body">
<form method="post" action="<?php echo base_url() ?>stock/stockPointVente" enctype="multipart/form-data">
<div>
<span><?php echo "Famille:"; ?></span>
<select name="familles" style="width:290px;">
<option value=""> - </option>
<?php
foreach ($labelFamille as $val) {
    if ($val['id_famille'] == $family) {
        echo '<option  selected value="'. $val['id_famille'].'">'. $val['label_famille'].'</option>';
    } else {
        echo '<option value="'. $val['id_famille'].'">'. $val['label_famille'].'</option>';
    }
} ?>
</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</div>
<br>
<span><?php echo "Prix de gros entre:&nbsp;"; ?></span>
<input type="number" step="0.001" id="prix_min"  name="prix_min" value="<?php echo $prix_gros_min; ?>" style="width:200px;"> et <input type="number" step="0.001" id="prix_max" value="<?php echo $prix_gros_max; ?>"  name="prix_max"  style="width:200px;"> <br><br>
<br>
<span><?php echo "Colisage CTN:&nbsp;"; ?></span>
<input type="text" id="colisage"  name="colisage" value="<?php echo $colisage; ?>" style="width:200px;">
&nbsp;&nbsp;&nbsp;&nbsp;<span>
<?php echo "Stock En Pièces:&nbsp;"; ?></span><input type="text" id="stock_pcs"  name="stock_pcs" value="<?php echo $stock_pcs; ?>" style="width:200px;">
<br><br>
<?php echo "Point de vente:&nbsp;"; ?></span>

<select name="id_point_vente" style="width:290px;">
<option value=""> - </option>
<option value="1"> Boutique Hamam-lif </option>
<option value="28"> boutik commession </option>
<option value="3"> boutik laouina </option>
<?php
//foreach ($listPoinVente as $val) {
//        echo '<option value="'. $val['id_pv'].'">'. $val['label_pv'].'</option>';
//} 
?>
</select>

<input type="submit" name="submit" value="Recherche de produits" class="btn btn-success" >
</form>
<br>
</div>
</div>
<form method="post" action="<?php echo base_url(); ?>commandes/commandes/commandeFromStock" enctype="multipart/form-data">
<table id="example" class="" cellspacing="0" width="90%">
<thead>
<tr>
<th></th>    
<th>Image</th><th>Nsfamille</th>
<th>Référence</th><th>Code à Barres</th><th>Designation</th><th>Prix DE GROS (HT)</th>
<th>Colisage CTN</th><th>Nombre Packets</th><th>Emballage</th><th>PRIX H,LIF</th><th>PRIX TUNIS</th>
<th>CTN DISPO</th><th>PCS DISPO</th><th>STOCK (PCS)</th>
</tr>
</thead>

<tbody>
<?php
if (empty($addressbook)){
    echo '<tr><td colspan="4">Aucun produit pour le moment</td></tr>';
}
else
{
?>


<?php    
    foreach ($addressbook as $key => $row) {
    echo'<tr>
            <td><input type="checkbox" name="choix['.trim($key).']" value="1" />
            <input type="hidden" name="reference['.trim($key).']" value="'.$addressbook[$key]['reference'].'" /></td>
            <td><img width="80" height="80" src="'.base_url().'images/products/'.$addressbook[$key]['reference'].'.jpg" /></td>
            <td>'. $addressbook[$key]['nsFamille'].'</td>
            <td>'. $addressbook[$key]['reference'].'</td>
            <td>'. $addressbook[$key]['codeBarre'].'</td>
            <td>'. $addressbook[$key]['designation'].'</td>
            <td>'. $addressbook[$key]['prixGrosHT'].'</td>
            <td>'. $addressbook[$key]['colisageCtn'].'</td>
            <td>'. $addressbook[$key]['colisagePacket'].'</td>
            <td>'. $addressbook[$key]['emballage'].'</td>
            <td>'. $addressbook[$key]['prixHlif'].'</td>
            <td>'. $addressbook[$key]['prixTunis'].'</td>
            <td>'. $addressbook[$key]['ctnDispo'].'</td>
            <td>'. $addressbook[$key]['pcsDispo'].'</td>
            <td>'. $addressbook[$key]['stockPcs'].'</td>
    </tr>';
}
}
?>
<br><br><br>	
<input type="submit" class="btn btn-success" name="submit" value="Envoyer" >
<input type="reset" name="Annuler">
</tbody>
</table>
</form>
</div>
</div>
</div>
</div>


</div></div>

</div><!-- #end div #main .wrapper -->
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/jquery/dist/jquery.min.js"></script>
<link type="text/css" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.8.20.custom.css" rel="Stylesheet" />
<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/jquery-ui.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
$(document).ready(function() {
    $('#example').DataTable({
        responsive: true,
        "order": [[ 1, "desc" ]]
    });
});
</script>
</html>
<?php // $this->load->view('template/footer_view_datatables.php'); ?>