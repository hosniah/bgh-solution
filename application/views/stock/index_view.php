<?php $this->load->view('template/header_beta_view.php'); ?>

<body>
<script src="<?php echo base_url();?>js/featherlight/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
<link href="<?php echo base_url();?>js/featherlight/release/featherlight.min.css" type="text/css" rel="stylesheet" />
<div id="wrapper">
<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
<?php
    $this->load->view('template/topbar');
    $this->load->view('template/sidebar');
?>
</nav>
<div id="page-wrapper">
<div class="row">
<!-- main content area -->  
<!-- main content area -->
<div class="col-lg-12">
<div class="panel-body">
<div class="dataTable_wrapper">
<?php if (isset($error)): ?>
    <div class="alert alert-error"><?php echo $error; ?></div>
<?php endif; ?>
<?php if ($this->session->flashdata('success') == TRUE): ?>
        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
<?php endif; ?>
<section id="page-header" class="clearfix">    
	<div class="wrapper">
		<h1><?php echo 'page Point de vente'/*$this->lang->line('h1_users_manage_add')*/; ?></h1>
    </div>

</section>


<!-- main content area -->   
<div class="wrapper" id="main"> 

	<!-- content area -->    
	<section id="content" class="wide-content">
		<a href="<?php echo base_url(); ?>stock/pointvente/edit/0"><?php echo $this->lang->line('new'); ?></a>
		<table>
			<tr>
                        <th><?php echo 'Boutique'; ?></th>
                        <th><?php echo 'Tel'; ?></th>
                        <th><?php echo 'Fax'; ?></th>
                        <th><?php echo 'Adresse'; ?></th>
                        <th><?php echo 'Email'; ?></th>
                        <th><?php echo 'Responsable'; ?></th>
                        <th><?php echo 'Status'; ?></th>
                        <th colspan="2"><?php echo $this->lang->line('h1_action_famille'); ?></th>
			<?php $i = 1; ?>
			<?php foreach ($listes as $liste) { ?>
				<tr>
					<td><?php echo $liste['label_pv']; ?></td>
					<td><?php echo $liste['tel_pv']; ?></td>								
					<td><?php echo $liste['fax_pv']; ?></td>
        				<td><?php echo $liste['adresse_pv']; ?></td>
                                        <td><?php echo $liste['email_pv']; ?></td>
                                        <td><?php echo $liste['resp_pv']; ?></td>
                                        <td><?php echo $liste['deleted_pv']; ?></td>

					<td>
						<a href="<?php echo base_url(); ?>stock/pointvente/del/<?php echo $liste['id_pv']; ?>" onclick="return confirm(<?php echo $this->lang->line('confirm_delete'); ?>);"><?php echo $this->lang->line('del'); ?></a>
						<a href="<?php echo base_url(); ?>stock/pointvente/edit/<?php echo $liste['id_pv']; ?>"><?php echo $this->lang->line('edit'); ?></a>
					</td>
				</tr>
			<?php } ?>			
		</table>
	</section><!-- #end content area -->

</div><!-- #end div #main .wrapper -->

</div>
</div>
</div>
</div>


</div></div>

</div><!-- #end div #main .wrapper -->
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/jquery/dist/jquery.min.js"></script>
<link type="text/css" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.8.20.custom.css" rel="Stylesheet" />
<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/jquery-ui.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
$(document).ready(function() {
    $('#example').DataTable({
        responsive: true,
        "order": [[ 1, "desc" ]]
    });
});
</script>
</html>
<?php // $this->load->view('template/footer_view_datatables.php'); ?>