<?php $this->load->view('template/header_beta_view.php'); ?>

<body>
<script src="<?php echo base_url();?>js/featherlight/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
<link href="<?php echo base_url();?>js/featherlight/release/featherlight.min.css" type="text/css" rel="stylesheet" />
<div id="wrapper">
<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
<?php
    $this->load->view('template/topbar');
    $this->load->view('template/sidebar');
?>
</nav>
<div id="page-wrapper">
<div class="row">
<!-- main content area -->  
<!-- main content area -->
<div class="col-lg-12">
<div class="panel-body">
<div class="dataTable_wrapper">
<?php if (isset($error)): ?>
    <div class="alert alert-error"><?php echo $error; ?></div>
<?php endif; ?>
<?php if ($this->session->flashdata('success') == TRUE): ?>
        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
<?php endif; ?>
<!-- content area -->
    <section id="content" class="wide-content">
    <table>
        <tr>
            <th><?php echo 'Image'; ?></th>
            <th><?php echo 'Réference'; ?></th>
            <th><?php echo 'Quantité'; ?></th>
            <th><?php echo 'Colisage'; ?></th>
            <th><?php echo 'CTN'; ?></th>
            <th><?php echo 'Commentaires'; ?></th>
            <th><?php echo 'Actions'; ?></th>
            <?php $i = 1; ?>
            <?php foreach ($details as $detail) { ?>
            <tr>
                <td><?php echo '<img height="80" width="80" src="'.base_url().'images/products/'. $detail['reference'].'.jpg"'; ?>/></td>
                <td><?php echo $detail['reference']; ?></td>
	        	<td><?php echo $detail['quantite']; ?></td>
                <td><?php echo $detail['colisage']; ?></td>
                <td><?php $ctn = $detail['quantite'] / $detail['colisage']; echo $ctn; ?></td>
                <td><?php echo $detail['commentaires']; ?></td>
                <td><a  href="<?php echo base_url().'stock/commande/deleteProduct/'.$detail['id']; ?>" class="btn btn-outline btn-danger" > Supprimer </a>
				    <a href="<?php echo base_url().'stock/commande/updateProduct/'.$detail['id']; ?>" data-toggle="modal" data-target="<?php echo "#myModal_".$detail['id']; ?>" class="btn btn-outline btn-success" > Modifier </a></td>
			</tr>
            <?php 
				echo '  <!-- Modal -->
                            <div class="modal fade" id="myModal_'.$detail['id'].'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Modifier la quantité de l\'article : '.  $detail['reference'].'</h4>
                                        </div>
                                        <div class="modal-body"><br>
							<form method="POST" action="'. base_url().'stock/commande/updateProduct" enctype="multipart/form-data">
								<input type="hidden" id="article_id" name="article_id" value="'. $detail['id'].'">
								<input type="hidden" id="commande_id" name="commande_id" value="'. $detail['id_commande'].'">
								Nouvelle quantité en CTN: &nbsp;<input type="text" id="quantite_ctn" name="quantite_ctn" value="'. $ctn.'"><br><br>
								Nouvelle quantité en PCS: &nbsp;<input type="text" id="quantite_pcs" name="quantite_pcs" value="'. $detail['quantite'].'">
								<br><HR><br>
								<button type="submit" class="btn btn-lg btn-success">Modifier la quantité</button>
							</form>
							
							</div></div></div></div>';
			}
			?>
    </table>
	<br>
	<?php
	echo '<a href="'.base_url().'stock/commande/depot_valider_commande/'.$detail['id_commande'].'" class="btn btn-lg btn-outline btn-danger"> Valider la commande </a> ';
	?>
    </section><!-- #end content area -->
<?php

if($cmd_originale_details) {
   echo ' <section id="content" class="wide-content">
		Commande originale enregistrée par l\'utilisateur:';
				$flag_commande_original_modified = false;
				//var_dump($cmd_originale_details);
				echo '<table>
						<th>Réference</th>
						<th>Quantité à compléter </th>
						<th>Commentaire </th>';
				echo '<form method="POST" action="'. base_url().'stock/commande/depot_completer_commande" enctype="multipart/form-data">';
			    echo '<input type="hidden" id="id_commande" name="id_commande" value="'. $detail['id_commande'].'">';
				foreach ($cmd_originale_details as  $key => $detail) {
					echo '<input type="hidden" id="reference_'.$key.'" name="reference_'.$key.'" value="'.$detail['reference'].'">';
					echo '<input type="hidden" id="quantite_'.$key.'" name="quantite_'.$key.'" value="'.$detail['quantite'].'">';
					echo '<tr>
						<td>'. $detail['reference'].'</td>
						<td>'. $detail['quantite'].'</td><td>'. $detail['commentaires'].'</td></tr>';
				}
				echo '<input type="hidden" id="size_commande" name="size_commande" value="'. sizeof($cmd_originale_details).'">';
				echo '</table><br>';
				echo '<br><button type="submit" class="btn btn-lg btn-success">Créer une commande de rappel à partir des quantités manquantes</button>';
				echo '</form>';
   
				//echo '<a href="'.base_url().'stock/commande/depot_completer_commande/'.$detail['id_commande'].'" class="btn btn-lg  btn-info"> Créer une commande de rappel à partir des quantités manquantes </a> ';
echo '</section>';
}
			?>
	

</div><!-- #end div #main .wrapper -->
</div>
</div>
</div>
</div>


</div></div>

</div><!-- #end div #main .wrapper -->
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/jquery/dist/jquery.min.js"></script>
<link type="text/css" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.8.20.custom.css" rel="Stylesheet" />
<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/jquery-ui.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>assets/startbootstrap-sb-admin-2-gh/dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
$(document).ready(function() {
    $('#example').DataTable({
        responsive: true,
        "order": [[ 1, "desc" ]]
    });
});
</script>
</html>
<?php // $this->load->view('template/footer_view_datatables.php'); ?>