<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Commandes extends CI_Controller {

    public function __construct() {
        parent::__construct();
//	if (!$this->session->userdata('user_lvl')) {
//            die(redirect(base_url() . 'main/noaccess'));
//	}
            $this->load->model('warehouse/library_model');
            $this->load->model('warehouse/log_model');
            $this->load->model('familles/familles_model');
            $this->load->model('warehouse/order_model');
            $this->load->model('stock/commande_model');

            $this->load->model('stock/stock_model');
	}

        public function index() {
        }

	/**
	 * Affichage des commandes en attente chez le depot à valider par l'agent du depot ou le super admin.
	 */
	public function encours() {
		$data = array();
		$status = '100';
		$id = $this->session->userdata('id_pointvente');//$this->session->userdata('user_id');
		$data['avalider'] = $this->order_model->getOrderCommandes($id, $status);
		$this->load->view('commandes/valider_agent_depot_view', $data);
	}

	/**
	 * Affichage des commandes livrées par le depot à valider par le responsable de la boutique.
	 */
	public function avalider() {
		$data             = array();
		$status           = '200';
		$id               = $this->session->userdata('id_pointvente');//$this->session->userdata('user_id');
		$data['avalider'] = $this->order_model->getOrderCommandes($id, $status);
                //        var_dump($data['avalider']);
		$this->load->view('commandes/a_valider_view', $data);
	}

       public function cmdValid() {
            if(!empty($this->input->post('id'))) {
                $id = $this->input->post('id');
                // data table order_products
                $data = array();
                $data['products'] = $this->commande_model->getListCommande($id);

                //data table products
                $validate = array();
                $i = 0;
                foreach ($data['products'] as $key => $value) {
                        $ref[$key] = $value['reference'];
                        $validate['listes'][$i] = $this->library_model->getProductValid($id, $ref[$key]);
                        $i++;
                }
				//var_dump($validate['listes']);
                $val = array();
                $j = 0;
                foreach($validate['listes'] as $key => $val) {
                    $ref = $val[0]['reference'];
                    $qte = $val[0]['quantite'];
                    $this->commande_model->updateValidCommandProduct($ref, $qte);
                }
                $this->commande_model->updateValidCommand($id);
				
               // redirect('commandes/a_valider_view');
            }
        }

		/**
		 * Validation totale / partielle d'une commande reçu par le point de vente
		 *
		 */
        public function validationPointDeVente_2() {
            if(!empty($this->input->post('id'))) {
                $id = $this->input->post('id');
                // data table order_products
                $data = array();
                $data['products'] = $this->commande_model->getListCommande($id);
               
                //data table products
                $validate = array();
                $i = 0;
                foreach ($data['products'] as $key => $value) {
                        $ref[$key] = $value['reference'];
                        $validate['listes'][$i] = $this->library_model->getProductValid($id, $ref[$key]);
                        $i++;
                }
		//var_dump($validate['listes']);
                $val = array();
                $j   = 0;
				if (!empty($validate['listes'])) {
					foreach($validate['listes'] as $key => $val) {
						$ref = $val[0]['reference'];
						$qte = $val[0]['quantite'];
						$this->commande_model->updateValidCommandProduct($ref, $qte);
						$id_point_vente = $this->session->userdata('id_pointvente');
                                                $product_details = $this->library_model->getProductIdFromReference($ref);                               
                                                $pr_id = $product_details[0]->{'id'};
                                                $det = $this->library_model->getProduct($pr_id);
                                               // var_dump($det);
                                                /*
						$this->stock_model->updatepointDeVenteStock($ref, $qte, $id_point_vente
                                                     $det['codeBarre'], $det['designation'],
					             $det['prixGrosHT'], $det['tva'],
				                     $det['unite'], $det['colisageCtn'],
                                                     $det['emballage'], $det['prixHlif'],
                                                     $det['prixTunis'], $det['ctnDispo'],
                                                     $det['pcsDispo'], $det['stockPcs'],
                                                     $det['cf'], $det['image']
					        ); */
                                               var_dump($det);
					}
					$this->commande_model->updateValidCommand($id);
				}
          //      die();
                redirect('commandes/commandes/historique');
            }
        }


        public function validationPointDeVente() {
            if(!empty($this->input->post('id'))) {
                $id = $this->input->post('id');
                // data table order_products
                $data = array();
                $data['products'] = $this->commande_model->getListCommande($id);
                //data table products
                $validate = array();
                $i = 0;
                foreach ($data['products'] as $key => $value) {
                        $ref[$key] = $value['reference'];
                        $validate['listes'][$i] = $this->library_model->getProductValid($id, $ref[$key]);
                        $i++;
                }
				//var_dump($validate['listes']);
                $val = array();
                $j   = 0;
				if (!empty($validate['listes'])) {
					foreach($validate['listes'] as $key => $val) {
						$ref = $val[0]['reference'];
						$qte = $val[0]['quantite'];
						$this->commande_model->updateValidCommandProduct($ref, $qte);
						$id_point_vente = $this->session->userdata('id_pointvente');
						$this->stock_model->updatepointDeVenteStock($ref, $qte, $id_point_vente);
					}
					$this->commande_model->updateValidCommand($id);
				}
                redirect('commandes/commandes/historique');
            }
        }
				
			public function historique() {
            $data   = array();
						$id     = $this->session->userdata('id_pointvente');
						$status = 400; //afficher uniquement les commandes acceptées
						$data['historique'] = $this->order_model->getOrderCommandes($id, $status);
						$this->load->view('commandes/historique_view', $data);
        }
        
        public function commandeFromStock() {
           
					//echo "<pre>";
					//var_dump($_POST);
					
					$this->load->view('commandes/fromStock_view');
					
        }



}
?>
