<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Vente extends CI_Controller {
	public $id_commande_client;

	public function __construct() {
		parent::__construct();
        $this->load->model('sales/sales_model');
        $this->load->model('warehouse/library_model');
	}

	private function init_vente() {
	        $vente                   = array();
		$vente['point_vente_id'] = $this->session->userdata('id_pointvente');
		$vente['user_id']        = $this->session->userdata('user_id');
		$vente['time']           = date('Y-m-d H:i:s');//time();
               // var_dump($vente['time']);
		$vente['sum']            = 0;
		$vente['type_vente']     = 1;
		
		$id_sale   = $this->sales_model->addVenteGros($vente);
		if (!empty($id_sale) && empty($this->session->userdata('id_sale'))) {
			$this->id_sale = $id_sale;
			$this->session->set_userdata('id_sale', $id_sale);
		} 
	}
	

    public function list_all() {
		$data = array();
		$id_pt_de_vente = 1;
		$data['vente']  =  $this->sales_model->getVentesByPointDeVenteId($id_pt_de_vente);
		
		$this->load->view('vente/list_vente_view', $data);
	}

    public function index() {
	
	
	

	
	
	$data = array();
	$this->session->unset_userdata('scannedArticles');
        $id_pt_vente = $this->session->userdata('id_pointvente');
        $vente_Details_number = $this->sales_model->countVenteDetail($id_pt_vente);
				
				$vente_Gros_number = $this->sales_model->countVenteGros($id_pt_vente);
	
				
        $data['countVenteManuelle'] = $vente_Details_number[0]['nb_ventes'];
				
				$data['countVenteGros'] = $vente_Gros_number[0]['nb_ventes'];
				
		$this->load->view('vente/default_vente_view', $data);
	}

	public function venteDetailsManuelle() {
        $data = array();
		if (!$this->session->userdata('id_sale')) {
		    $this->init_vente();
			//die(redirect(base_url()));
		}
		$data['id_sale'] = $this->session->userdata('id_sale');
		//charge les produits pour l'autocomplete
		$data['products_list'] = $this->getProductsAsJson();
		//var_dump($data['products_list']);
 
		//an article is added
        if(!empty($this->input->post('token')) && !empty($this->input->post('txtQte'))) {
			$data['barcode'] = $data['reference'] = '';
		    if (!empty($this->input->post('txtRef'))) {
				$data['reference'] = $this->input->post('txtRef'); 
			}
			if(!empty($this->input->post('txtBarcode'))) {
				$data['barcode']   = $this->input->post('txtBarcode');
			}
			$data['quantite']  = $this->input->post('txtQte');
			$this->facturation_details_update($data['id_sale'], $data['reference'], $data['quantite'], $data['barcode']);
		}
		
		$data['details'] = $this->facturation_details_load($data['id_sale']);
		//$data['details'] = $this->commande_model->getListCommande($id);
        $this->load->view('vente/cashdesk_detail_manuelle_view.php', $data);
	}

	/**
	 * Retrive products from Pitn de vente warehouse
	 *
	 */
	public function getProductsAsJson() {
		$json = array();
		$all_products = $this->sales_model->get_product_refs_for_autocomplete();
		foreach ($all_products as $row) {
			$json[] = $row['reference'];
		}
		$a = json_encode($json);
		return $a;
	}
	
    public function venteGros() {
        $data = array();
		if (!$this->session->userdata('id_sale')) {
			$this->init_vente();
			//die(redirect(base_url()));
		}
		$data['id_sale'] = $this->session->userdata('id_sale');
		//var_dump($this->session->userdata('id_sale'));
		$data['products_list'] = $this->getProductsAsJson();
		//var_dump($data['products_list']);
 
		//an article is added
        if(!empty($this->input->post('token')) && !empty($this->input->post('txtQte'))) {
			$data['barcode'] = $data['reference'] = '';
		    if (!empty($this->input->post('txtRef'))) {
				$data['reference'] = $this->input->post('txtRef'); 
			}
			if(!empty($this->input->post('txtBarcode'))) {
				$data['barcode']   = $this->input->post('txtBarcode');
			}
			$data['quantite']  = $this->input->post('txtQte');
			$this->facturation_gros_update($data['id_sale'], $data['reference'], $data['quantite'], $data['barcode']);
		}
		
		$data['details'] = $this->facturation_gros_load($data['id_sale']);
		//$data['details'] = $this->commande_model->getListCommande($id);
        $this->load->view('vente/cashdesk_gros_view', $data);
	}

    public function doCreateVenteDetails() {
		$data = array();

		$this->load->model('factures/facture_model');
		$data['id_sale'] = $this->session->userdata('id_sale');
		//var_dump($data['id_sale'] );

		$articles = $this->input->post('scannedArticles');
		if(!empty($articles)) {
			if (empty($this->session->userdata('scannedArticles'))) {
				$data['details'] = array();
			} else {
				$Data = $this->session->userdata('scannedArticles');
			}
			$myArticlesArray = explode(',', $articles);
			// Enforce vente as detail (instead of Gros)
			$this->sales_model->enforceVenteAsDetail($data['id_sale']);
			foreach($myArticlesArray as $key =>$barcode){
				$barcode = trim($barcode);
				if(!empty($barcode)) {
					$details = $this->sales_model->get_product_details_by_barcode($barcode);
					if (!empty($details)) {
						$data['details'][$key]['reference'] = $details['0']['reference'];
						$data['details'][$key]['prixTunis'] = $details['0']['prixTunis'];
						$data['details'][$key]['prixHlif']  = $details['0']['prixHlif'];
						$data['details'][$key]['stockPcs']  = $details['0']['stockPcs'];
						$data['details'][$key]['codeBarre'] = $details['0']['codeBarre'];
						if ($Data) {
							$data['details'] = array_merge($data['details'],$Data);
						}
					}
				}
			}
			//Cleanup session
			//$this->session->unset_userdata('id_sale');
			$this->session->set_userdata('scannedArticles', $data['details']);
		} 
		$this->load->view('vente/cashdesk_detail_view', $data);
	}

	public function validateVenteDetails() {
		//var_dump("Nouvelle vente details enregistrée !!!!!!");
		$data['id_sale'] = $this->session->userdata('id_sale');
		if(!empty($data['id_sale']) ) {
			$articles = $this->session->userdata('scannedArticles');
			$id_sale           = $this->input->post('id_sale');
			$computed_total_ht = $this->input->post('computed_total_ht');
			$txtEncaisse       = $this->input->post('txtEncaisse');
			$txtDu             = $this->input->post('txtDu');

			foreach ($articles as $key=>$row) {
				$txtDu += $row['prixTunis'];
				//var_dump($row['prixTunis']);
			}
			//var_dump($key);
			//var_dump($txtDu);
			
			$data['total_ht']   = $computed_total_ht;
			$data['total_ttc']  = $txtDu;
			$data['encaisse']   = $txtEncaisse;
			$data['facnumber']  =  "BGH-".$id_sale;

			
			$data['details'] = $this->facturation_gros_load($id_sale);
			
			// Get all article in commande
			// Save sale in database (paiement, status, articles, etc...)
			// Cleanup session  since sale has been done
			/* Steps Goes here */

		    //Facture details creation
		    $this->load->model('factures/facture_model');
		    $data_facture = array();
			if(!empty($data['total_ttc'])) {
				$data_facture['amount']     = $data['total_ttc'];
				$data_facture['paye']       = 1;
				$data_facture['facnumber']  = $data['facnumber'];
				$data_facture['note']       = $this->input->post('txtaNotes');
				$this->facture_model->add($data_facture);

			     //$this->stock_model->updateStockAfterSale($ref, $id_ptDeVente);
				//Cleanup session
				$this->session->unset_userdata('id_sale');
			}

			// Validate Sale status first
			//$this->sales_model->validateSale($id_sale);

			$this->load->view('vente/facture_vente_view', $data);
		}
	}

    public function doCreateVenteGros() {
		if(!empty($this->input->post('id_sale')) && !empty($this->input->post('computed_total_ht'))) {
			$id_sale           = $this->input->post('id_sale');
			//var_dump($id_sale);
			$computed_total_ht = $this->input->post('computed_total_ht');
			//var_dump($computed_total_ht);
			$txtEncaisse       = $this->input->post('txtEncaisse');
			//var_dump($txtEncaisse);
			$txtDu             = $this->input->post('txtDu');
			//var_dump($txtDu);
			// Get all article in commande
			// Save sale in database (paiement, status, articles, etc...)
			// Cleanup session  since sale has been done
			/* Steps Goes here */

			// Validate Sale status first
			$this->sales_model->validateSale($id_sale);
			//save total HT in sales db
			$this->sales_model->updateCommandeTTC($id_sale, $computed_total_ht);
			//Save sale paiement in database
			$data = array();
			$data['total_ht']  = $computed_total_ht;
			$data['total_ttc'] = $txtDu;
			$data['encaisse']  = $txtEncaisse;
			$data['facnumber']  =  "BGH-".$id_sale;
			
			
			$data['details'] = $this->facturation_gros_load($id_sale);
			
		//Facture details creation
		$this->load->model('factures/facture_model');
		$data_facture = array();
		if(!empty($data['total_ttc'])) {
			$data_facture['amount']     = $data['total_ttc'];
			$data_facture['paye']       = 1;
			$data_facture['facnumber']  = $data['facnumber'];
			$data_facture['note']  = $this->input->post('txtaNotes');
			$this->facture_model->add($data_facture);

			//Cleanup session
			$this->session->unset_userdata('id_sale');
		}
			$this->load->view('vente/facture_vente_view', $data);
			//Cleanup session
			//$this->session->unset_userdata('id_sale');
			//at the end, redirect to the list of commandes interface
			//redirect('vente/vente/list_all');
		}
	}
    public function facturation_gros_load($id_sale) {
        $product_details = $this->sales_model->lod_sales_details($id_sale);
		//var_dump($product_details);
		foreach ($product_details as $key=>$row) {
			$product_stuff = $this->library_model->getProduct($product_details[$key]['product_id']);
			$product_details[$key]['ref'] = $product_stuff['reference'];
		}
		return $product_details;
	}

	
 /**
  * Inteface de recap de vente manuelle detail (avec bouton d'impression de ticket de caisse).
  *
  */
  public function doCreateVenteManuelleDetails() {
		if(!empty($this->input->post('id_sale')) && !empty($this->input->post('computed_total_ht'))) {
			$id_sale           = $this->input->post('id_sale');
			
			//var_dump($id_sale);
			$computed_total_ht = $this->input->post('computed_total_ht');
			//var_dump($computed_total_ht);
			$txtEncaisse       = $this->input->post('txtEncaisse');
			//var_dump($txtEncaisse);
			$txtDu             = $this->input->post('txtDu');
			//var_dump($txtDu);

			// Get all article in commande
			// Save sale in database (paiement, status, articles, etc...)
			// Cleanup session  since sale has been done
			/* Steps Goes here */
			
			$this->sales_model->enforceVenteAsDetail($id_sale);

			// Validate Sale status first
			$this->sales_model->validateSale($id_sale);
			//save total HT in sales db
			$this->sales_model->updateCommandeTTC($id_sale, $computed_total_ht);
			//Save sale paiement in database
			$data = array();
			$data['total_ht']  = $computed_total_ht;
			$data['total_ttc'] = $txtDu;
			$data['encaisse']  = $txtEncaisse;
			$data['facnumber']  =  "BGH-".$id_sale;
			
			
			$data['details'] = $this->facturation_gros_load($id_sale);
			
		//Facture details creation
		$this->load->model('factures/facture_model');
		$data_facture = array();
		if(!empty($data['total_ttc'])) {
			$data_facture['amount']     = $data['total_ttc'];
			$data_facture['paye']       = 1;
			$data_facture['facnumber']  = $data['facnumber'];
			$data_facture['note']  = $this->input->post('txtaNotes');
			$this->facture_model->add($data_facture);

			//Cleanup session
			$this->session->unset_userdata('id_sale');
		}
			$this->load->view('vente/facture_vente_view', $data);
			//Cleanup session
			//$this->session->unset_userdata('id_sale');
			//at the end, redirect to the list of commandes interface
			//redirect('vente/vente/list_all');
		}
	}

    public function facturation_details_load($id_sale) {
        $product_details = $this->sales_model->lod_sales_details($id_sale);
		//var_dump($product_details);
		foreach ($product_details as $key=>$row) {
			$product_stuff = $this->library_model->getProduct($product_details[$key]['product_id']);
			$product_details[$key]['ref'] = $product_stuff['reference'];
		}
		return $product_details;
	}

    public function facturation_gros_update($id_sale, $ref, $qte, $barcode=NULL) {
		$vente = array();
        if(!empty($qte) && !empty($ref)) {
				$vente['quantity'] = $qte;
				$vente['sale_id'] = $id_sale;
				$product_details = $this->sales_model->get_product_details_by_ref($ref); //var_dump($product_details);
				$vente['product_id']    = $product_details[0]['id'];
				$vente['current_price'] = $product_details[0]['prixGrosHT'];
				//pcsDispo
				$id_sale_details = $this->sales_model->addSaleDetailItem($vente);
				return $id_sale_details;
		}
	}

    public function facturation_details_update($id_sale, $ref, $qte, $barcode=NULL) {
		$vente = array();
        if(!empty($qte) && !empty($ref)) {
				$vente['quantity'] = $qte;
				$vente['sale_id'] = $id_sale;
				$product_details = $this->sales_model->get_product_details_by_ref($ref);
				$vente['product_id']    = $product_details[0]['id'];
				$vente['current_price'] = $product_details[0]['prixTunis'];
				//pcsDispo
				$id_sale_details = $this->sales_model->addSaleDetailItem($vente);
				return $id_sale_details;
		}
	}

    public function venteDetails() {
        $data = array();
		if (!$this->session->userdata('id_sale')) {
		    $this->init_vente();
			//die(redirect(base_url()));
		}
                //$data['details'] = $this->commande_model->getListCommande($id);
                $this->load->view('vente/cashdesk_detail_view', $data);
    }


    public function remove_item() {
		$id = $this->uri->segment(4);
                $this->sales_model->deleteSaleDetailItem($id);

		
                /*$data = array();                                      
                $this->load->view('vente/cashdesk_detail_view', $data);*/
                $this->venteDetailsManuelle();
    }
	/*
        public function add() {
                if(!empty($this->input->post('id'))) {
                    $id = $this->input->post('id');

                    // data table order_products
                    $data = array();
                    $data['products'] = $this->commande_model->getListCommande($id);

                    //data table products
                    $validate = array();
                    $i = 0;
                    foreach ($data['products'] as $key => $value) {
                            $ref[$key] = $value['reference'];
                            $validate['listes'][$i] = $this->library_model->getProductValid($id, $ref[$key]);
                            $i++;
                    }

                    $val = array();
                    $j = 0;
                    foreach($validate['listes'] as $key => $val) {
                        $ref = $val[0]['reference'];
                        $qte = $val[0]['quantite'];
                        $this->valid_product_model->updateValidCommandProduct($ref, $qte);
                    }
                    $this->valid_product_model->updateValidCommand($id);

                    redirect('stock/commande');
                }
        }
		*/
		public function updateQuantity() {
			if(!empty($this->input->post('id_sales_detail'))) {
					$data = array();
					$id_sale           = $this->input->post('id_sales_detail');
					$qte           = $this->input->post('qte');

					// update quantite sales
					$this->sales_model->updateArticleQte($id_sale, $qte);
					$this->load->view('vente/cashdesk_detail_manuelle_view.php', $data);
			}
		}
		
		public function updatePrice() {
			if(!empty($this->input->post('id_sales_detail'))) {
					$data = array();
					$id_sale           = $this->input->post('id_sales_detail');
					$price           = $this->input->post('price');

					// update quantite sales
					$this->sales_model->updateArticlePrice($id_sale, $price);
					$this->load->view('vente/cashdesk_detail_manuelle_view.php', $data);
			}
		}
}