<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class ListProducts extends CI_Controller {

	public function __construct() {
		parent::__construct();
//		if (!$this->session->userdata('user_lvl')) {
//			die(redirect(base_url() . 'main/noaccess'));
//		}
		$this->load->model('warehouse/library_model');
		$this->load->model('warehouse/log_model');
		$this->load->model('warehouse/order_model');
                $this->load->model('familles/familles_model');
	
        $this->load->model('csv_model');
        $this->load->library('csvimport');
	}

	public function index() {
		/*if (!$this->session->userdata('admin_lvl')) {
			die(redirect(base_url() . 'main/noaccess'));
		}*/
		$data = array();
		$family=NULL; $sub_family=NULL;
		$data['prix_gros_min'] = ''; 
		$data['prix_gros_max'] = ''; $data['sub_family'] = ''; $data['family'] = '';$data['stock_pcs'] ='';$data['colisage'] = '';
		$colisage_ctn=NULL; $stock_pcs=NULL;

			$data['labelFamille'] = $this->familles_model->getList();
			$data['sousfamilles'] = $this->familles_model->getSousList();
			if(!empty($this->input->post('sousFamilles'))) {
				$data['addressbook'] = $this->library_model->getProductFamily($this->input->post('sousFamilles'));
			} else {
				$data['addressbook'] = $this->csv_model->get_addressbook();
			}
        if(!empty($this->input->post('familles'))) {
			$data['family'] = $this->input->post('familles');
		}
        if(!empty($this->input->post('sousFamilles'))) {
			$data['sub_family'] = $this->input->post('sousFamilles');
		}
        if(!empty($this->input->post('prix_min'))) {
			 $data['prix_gros_min'] = $this->input->post('prix_min');
		}
        if(!empty($this->input->post('prix_max'))) {
			$data['prix_gros_max'] = $this->input->post('prix_max');
		}
        if(!empty($this->input->post('stock_pcs'))) {
			$data['stock_pcs'] = $this->input->post('stock_pcs');
		}
        if(!empty($this->input->post('colisage'))) {
			$data['colisage'] = $this->input->post('colisage');
		}

		$data['addressbook'] = $this->library_model->load($data['family'], $data['sub_family'], $data['prix_gros_min'] , $data['prix_gros_max'], $data['colisage'], $data['stock_pcs']);
			$this->load->view('dashboard_view', $data);
	}

	// id = 1
	public function import() {
		/*if (!$this->session->userdata('admin_lvl')) {
			die(redirect(base_url() . 'main/noaccess'));
		}*/
                    $data = array();
                    $data['labelFamille'] = $this->familles_model->getList();
                    $data['sousfamilles'] = $this->familles_model->getSousList();
                    if(!empty($this->input->post('sousFamilles')))
                    {
                        $data['addressbook'] = $this->library_model->getProductFamily($this->input->post('sousFamilles'));
                    } else {    
                        $data['addressbook'] = $this->csv_model->get_addressbook();
                    }                    
                    $this->load->view('warehouse/add/import_view', $data);
	}
        
        public function importSubmit() {
		/*if (!$this->session->userdata('admin_lvl')) {
			die(redirect(base_url() . 'main/noaccess'));
		}*/
		$data = array();
                $data['addressbook'] = $this->csv_model->get_addressbook();
                $data['productFamily'] = $this->csv_model->getProductFamily($id);

		$this->load->view('warehouse/add/import_view', $data);
	}

    function importcsv() {
        $data['addressbook'] = $this->csv_model->get_addressbook();
        $data['error'] = '';    //initialize image upload error array to empty

        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'csv';
        $config['max_size'] = '1000';
        $this->load->library('upload', $config);

        // If upload failed, display error
        if (!$this->upload->do_upload()) {
            $data['error'] = $this->upload->display_errors();

			$this->load->view('warehouse/add/import_view', $data);
        } else {
            $file_data = $this->upload->data();
            $file_path =  './uploads/'.$file_data['file_name'];
            if ($this->csvimport->get_array($file_path)) {
                $csv_array = $this->csvimport->get_array($file_path);
                foreach ($csv_array as $key => $row) {
					//$product_items = explode(",", $row['reference,name,id_importateur,prix_gros,prix_det_1,prix_det_2']);
                    $insert_data = array(
                        'image'          => $row['image'],
                        'nsFamille'      => $row['nsFamille'],
                        'reference'      => $row['reference'],
                        'codeBarre'      => $row['codeBarre'],
                        'designation'    => $row['designation'],
                        'prixGrosHT'     => $row['prixGrosHT'],
                        'tva'            => $row['tva'],
                        'unite'          => $row['unite'],
                        'colisageCtn'    => $row['colisageCtn'],
                        'emballage'      => $row['emballage'],
                        'prixHlif'       => $row['prixHlif'],
                        'prixTunis'      => $row['prixTunis'],
                        'ctnDispo'       => $row['ctnDispo'],
                        'pcsDispo'       => $row['pcsDispo'],
                        'stockPcs'       => $row['stockPcs'],
                        'cf'             => $row['cf'],
                        'deleted'        => 0,
                    );
                    $this->csv_model->insert_csv($insert_data);
                }
                $this->session->set_flashdata('success', 'Csv Data Imported Succesfully');
                redirect(base_url().'warehouse/add/import');
                //echo "<pre>"; print_r($insert_data);
            } else 
                $data['error'] = "Error occured";
	         	$this->load->view('warehouse/add/import_view', $data);
            }
            
        } 

	// id = 1
	public function production() {
		/*if (!$this->session->userdata('admin_lvl')) {
			die(redirect(base_url() . 'main/noaccess'));
		}*/
		$data = array();
		if ($this->input->post('sent')) {
			$this->log_model->addAction($this->input->post('id'), $this->input->post('amount'), 1, 0, $this->input->post('oid'));
		}

		$data['products'] = $this->library_model->getList();
		$data['reports'] = $this->log_model->getOrderList(1);
		$data['orders'] = $this->order_model->getList();

		$this->load->view('warehouse/add/production_view', $data);
	}

	public function returner() {
		$data = array();
		$data['products'] = $this->library_model->getList();

		if ($this->input->post('sent')) {
			$this->log_model->addAction($this->input->post('id'), $this->input->post('amount'), 2);
			redirect('enc/status', 'refresh');
		} else {
			$this->load->view('warehouse/add/returner_view', $data);
		}
	}

	public function admin() {
		$data = array();
		$data['products'] = $this->library_model->getList();

		if ($this->input->post('sent')) {
			$this->log_model->addAction($this->input->post('id'), $this->input->post('amount'), 3);
			redirect('enc/status', 'refresh');
		} else {
			$this->load->view('warehouse/add/admin_view', $data);
		}
	}

	public function sent() {
		$data = array();
		$data['products'] = $this->library_model->getList();

		if ($this->input->post('sent')) {
			$this->log_model->addAction($this->input->post('id'), (0 - $this->input->post('amount')), 4);
			redirect('warehouse/add/sent', 'refresh');
		} else {
			$this->load->view('warehouse/add/sent_view', $data);
		}
	}

	// id = 5
	public function onway() {
		$data = array();
		$amount = 0 - $this->input->post('amount');
		if ($this->input->post('sent')) {
			$this->log_model->addAction($this->input->post('id'), $amount, 5, $this->input->post('pckgid'));
		}

		$data['products'] = $this->library_model->getList();
		$data['packings'] = $this->library_model->getPackingsList();
		$data['reports'] = $this->log_model->getPackingList(5, 6);

		$this->load->view('warehouse/add/onway_view', $data);
	}

	// id = 5 + bloack 1 list
	public function grouponway() {
		$data = array();

		if ($this->input->post('pckgid')) {
			if ($this->input->post('sent') && $this->input->post('id') && $this->input->post('amount')) {
				$amount = 0 - $this->input->post('amount');
				$this->log_model->addAction($this->input->post('id'), $amount, 5, $this->input->post('pckgid'));
			}

			$data['products'] = $this->library_model->getList();
			$data['packings'] = $this->library_model->getPackingsList();
			$data['reports'] = $this->log_model->getPackingList(5, 6);
			$data['lock'] = $this->input->post('pckgid');

			$this->load->view('warehouse/add/grouponway_lock_view', $data);
		} else {
			$data['packings'] = $this->library_model->getPackingsList();
			$data['reports'] = $this->log_model->getPackingList(5, 6);
			$this->load->view('warehouse/add/grouponway_pre_view', $data);
		}
	}

	// id = 6
	public function packing() {
		$data = array();
		$amount = $this->input->post('amount');
		if ($this->input->post('sent')) {
			$this->log_model->addAction($this->input->post('id'), $amount, 6, $this->input->post('pckgid'));
		}

		$data['products'] = $this->library_model->getList();
		$data['packings'] = $this->library_model->getPackingsList();
		$data['reports'] = $this->log_model->getPackingList(6, 0);

		$this->load->view('warehouse/add/packing_view', $data);
	}

	// id = 6
	public function grouppacking() {
		$data = array();

		if ($this->input->post('pckgid')) {
			if ($this->input->post('sent') && $this->input->post('id') && $this->input->post('amount')) {
				$amount = $this->input->post('amount');
				$this->log_model->addAction($this->input->post('id'), $amount, 6, $this->input->post('pckgid'));
			}

			$data['products'] = $this->library_model->getList();
			$data['packings'] = $this->library_model->getPackingsList();
			$data['reports'] = $this->log_model->getPackingList(6, 0);
			$data['lock'] = $this->input->post('pckgid');

			$this->load->view('warehouse/add/grouppacking_lock_view', $data);
		} else {
			$data['packings'] = $this->library_model->getPackingsList();
			$data['reports'] = $this->log_model->getPackingList(6, 0);
			$this->load->view('warehouse/add/grouppacking_pre_view', $data);
		}
	}

	// id = 6
	public function autopacking() {
		$data = array();
		$data['auto'] = true;

		if ($this->input->post('pckgid')) {

			$packing_id = $this->input->post('pckgid');
			$this->log_model->copyActionByPacking(5, 6, $packing_id);

			$data['products'] = $this->library_model->getList();
			$data['packings'] = $this->library_model->getPackingsList();
			$data['reports'] = $this->log_model->getPackingList(6, 0);
			$data['lock'] = $this->input->post('pckgid');

			$this->load->view('warehouse/add/grouppacking_lock_view', $data);
		} else {
			$data['packings'] = $this->library_model->getPackingsList();
			$data['reports'] = $this->log_model->getPackingList(6, 0);
			$this->load->view('warehouse/add/grouppacking_pre_view', $data);
		}
	}

}
