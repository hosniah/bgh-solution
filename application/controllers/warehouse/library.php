<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Library extends CI_Controller {

	public function __construct() {
		parent::__construct();
	/*	if (!$this->session->userdata('admin_lvl')) {
			die(redirect(base_url() . 'main/noaccess'));
		}*/
		$this->load->model('warehouse/library_model');
		$this->load->model('stock/stock_model');
	}

	public function index() {
		$this->edit(0);
	}

	public function ajaxGetPointDeVentesotckByRef() {
		//cleanup the loop, it's crappy!
		if ($ref = $this->input->post('reference')) {
			$id_ptDeVente = $this->input->post('id_pt_vente');
			//we should use a method that retrieves sock from point de vente not from warehouse stock
			$tty = $this->stock_model->getProductStockByPtDeVente($ref, $id_ptDeVente);
			foreach ($tty as $stock) {
				$ajax_stock = $stock->{'stock_total'};
			}
			echo $ajax_stock;
		}
	}

	public function ajaxGetsotckByRef() {
		//cleanup the loop, it's crappy!
		if ($ref = $this->input->post('reference')) {
			//we should use a method that retrieves sock from point de vente not from warehouse stock
			$tty = $this->library_model->getProductStock($ref);
			foreach ($tty as $stock) {
				$ajax_stock = $stock->{'stock_total'};
			}
			echo $ajax_stock;
		}
	}

	public function edit($id) {
		$data = array();
		$data['products'] = $this->library_model->getList();
		if ($id == 0) {
			// add new	
			if ($this->input->post('sent')) {
				$this->library_model->addProductStock($this->input->post('name'), $this->input->post('desc'));
				redirect('warehouse/library', 'refresh');
			} else {
				$this->load->view('warehouse/edit_view', $data);
			}
		} else {
			// edit
			if ($this->input->post('sent')) {
				$this->library_model->updateProductStock($id, $this->input->post('name'), $this->input->post('desc'));
				redirect('warehouse/library', 'refresh');
			} else {
				$data['article'] = $this->library_model->getProduct($id);
				$this->load->view('warehouse/edit_view', $data);
			}
		}
	}

	public function del($id) {
		/*if (!$this->session->userdata('admin_lvl')) {
			die(redirect(base_url() . 'main/noaccess'));
		}*/
		$this->library_model->delProduct($id);
		redirect('warehouse/library', 'refresh');
	}

}
