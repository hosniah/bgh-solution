<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Order extends CI_Controller {

	public function __construct() {
		parent::__construct();
		/*if (!$this->session->userdata('admin_lvl')) {
			die(redirect(base_url() . 'main/noaccess'));
		}*/
		$this->load->model('warehouse/order_model');
		$this->load->model('warehouse/library_model');
		$this->load->model('warehouse/temporders_model');
	}

	public function index() {
		$data = array();
		$data['orders'] = $this->order_model->getList();
		//var_dump($data['orders'] );
		$this->load->view('admin_orders_reports_view', $data);
		//$this->edit(0);
	}

	public function waitingForValidation() {
		$data = array();
		$data['orders'] = $this->order_model->getListCommmandForAgenDepotByStatus("100");
		//var_dump($data['orders'] );
		$this->load->view('admin_orders_reports_view', $data);
		//$this->edit(0);
	}

	public function createOrder() {
		if ($user_id = $this->input->post('user_id')) {
			// create commande
			$currentDateTime = date('Y-m-d');
			$new_order       = array("status" => 100,  "id_client" => $user_id, "id_point_vente" => $this->session->userdata('id_pointvente') , "date_commande" => $currentDateTime,"description"=> '', "deleted" => 0);
			$this->order_model->createOrder($new_order);
			$createdOrder = $this->order_model->getInsertID();
		    $order_id     = $createdOrder[0]['MAX(id)'];
			$temp_order   = $this->temporders_model->getOrderList($user_id);
			foreach ($temp_order as $product) {
				$order_products = array();
				$order_products['id_commande']   = $order_id;
				$order_products['reference']     = $product['reference'];
				$order_products['quantite']      = $product['sum'];
				$order_products['commentaires']  = "";
				$this->order_model->createOrderProducts($order_products);
                //keeping track of original command details of user
				$order_products['id_user']  = $user_id;
				$order_products['status']  = 100;
				$this->order_model->keepTrackOfOriginalOrderProducts($order_products);
			}
			$this->temporders_model->deleteTempOrderProductsFoUser($user_id);
			
			// create products for new commande
		
			//@TODO delete temp products
		
			// @TODO redirect
			//redirect('warehouse/order/add');
			$data['orders'] = $this->order_model->getList($user_id);
			redirect('commandes/commandes/encours');
		}
	}

	public function add() {
		$userID = $this->session->userdata('user_id');
		$this->load->model('warehouse/library_model');
		$this->load->model('warehouse/packing_model');
		$this->load->model('users/manage_model');

		/*if (!$this->session->userdata('admin_lvl')) {
			die(redirect(base_url() . 'main/noaccess'));
		}*/
		$data = array();
		if ($this->input->post('sent')) {
			
                        $reference   = $this->input->post('id'); var_dump($reference);
		   	$id_product  = $this->library_model->getProductIdFromReference($reference);
		        $idProd      = $id_product[0]->{'id'};
			$this->temporders_model->addAction($idProd, $this->input->post('amount'), $userID, 0, 0);
		}
		$data['products_list'] = $this->getProductsAsJson();
		$data['products']      = $this->library_model->getList();
		$data['reports']       = $this->temporders_model->getOrderList($userID);
		//$data['orders']      = $this->order_model->getList();
		$data['orders']        = $this->temporders_model->getUserTempOrder($userID);
		$this->load->view('order/orderadd_view', $data);
	}
	
		public function delTempOrders($id) {
			$id = $this->uri->segment(4);
			$this->temporders_model->deleteTempOrder($id);

			redirect('warehouse/order/add', 'refresh');
	}
	
	public function delAllTempOrders() {
		$id     = $this->session->userdata('user_id');
		$this->temporders_model->deleteAllTempOrder($id);

		redirect('warehouse/order/add', 'refresh');
	}

	public function getProductsAsJson() {
		$this->load->model('sales/sales_model');
		$json = array();
		$all_products = $this->sales_model->get_product_refs_for_autocomplete();
		foreach ($all_products as $row) {
			$json[] = $row['reference'];
		}
		$a = json_encode($json);
		return $a;
	}

	public function edit($id) {
		$data           = array();
		$data['orders'] = $this->order_model->getList();
		if ($id == 0) {
			// add new	
			if ($this->input->post('sent')) {
				$this->order_model->addOrder($this->input->post('name'), $this->input->post('quantite'));
				redirect('warehouse/order', 'refresh');
			} else {
				$this->load->view('warehouse/orderedit_view', $data);
			}
		} else {
			// edit
			if ($this->input->post('sent')) {
				$this->order_model->updateOrder($id, $this->input->post('name'), $this->input->post('quantite'));
				redirect('warehouse/order', 'refresh');
			} else {
				$data['user'] = $this->order_model->getOrder($id, $status);
				$this->load->view('warehouse/orderedit_view', $data);
			}
		}
	}

	public function del($id) {
		/*if (!$this->session->userdata('admin_lvl')) {
			die(redirect(base_url() . 'main/noaccess'));
		}*/
		$this->order_model->delOrder($id);
		redirect('warehouse/order', 'refresh');
	}

	public function bonsortie($id) {
		$data = '';
		$commande = $this->order_model->getOrder($id);
		//var_dump($commande);
		
		
		foreach($commande['order_details'] as $key => $row) {
			//$tty = $this->library_model->getProductColisage($row->{'reference'});
			//var_dump($commande['order_details'][$key]);
		   // $commande['order_details'][$key]->{'colisage'} = "11";
		  
		}
		

		/*
		  'id' => string '15' (length=2)
  'status' => string '200' (length=3)
  'id_client' => string '1' (length=1)
  'date_commande' => string '2015-11-30 00:00:00' (length=19)
  'description' => string '' (length=0)
  'deleted' => string '0' (length=1)
  'order_details' => 
    array (size=4)
      0 => 
        object(stdClass)[24]
          public 'id' => string '1' (length=1)
          public 'id_commande' => string '15' (length=2)
          public 'reference' => string '58252' (length=5)
          public 'quantite' => string '30' (length=2)
          public 'commentaires' => string '' (length=0)
		*/
		$data['id']            = $commande['id'] ;
		// call getClientById($commande['id_client']);
		$data['client']        = $commande['id_client'] ;
                $data['point_vente']        = $commande['id_point_vente'] ;
		$data['date_commande'] = $commande['date_commande'] ;
		$data['order_details'] = $commande['order_details'] ;
		$this->load->view('template/bon_de_sortie_pint_view', $data);
	}

}
