<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class SousFamille extends CI_Controller {

	public function __construct() {
		parent::__construct();
//		if (!$this->session->userdata('user_lvl')) {
//			die(redirect(base_url() . 'main/noaccess'));
//		}
		$this->load->model('familles/familles_model');
	}

	public function index() {
		$data = array();
		$data['sousFamilles'] = $this->familles_model->getSousList();
		$this->load->view('familles/sousFamilles_view', $data);
	}

	public function edit($id) {
		$data = array();
		if ($id == 0) {
			// add new	
			if ($this->input->post('sent')) {
				$this->familles_model->addSousFamille($this->input->post('famille'), $this->input->post('label'), $this->input->post('status'));
				redirect('familles/sousFamille', 'refresh');
			} else {
				$data['sousFamilles'] = $this->familles_model->getSousList();
				$this->load->view('familles/editSousFamille_view', $data);
			}
		} else {
			// edit
			if ($this->input->post('sent')) {
				$this->familles_model->updateSousFamille($id, $this->input->post('famille'), $this->input->post('label'), $this->input->post('status'));
				redirect('familles/sousFamille', 'refresh');
			} else {
                            $data['familles'] = $this->familles_model->getList();
                            $id = $this->uri->segment(4);
                            $data['sousFamilles'] = $this->familles_model->getSousFamille($id);  
                            $this->load->view('familles/editSousFamille_view', $data);
				
                            $data['list'] = $this->familles_model->getSousList();
                            $this->load->view('familles/editSousFamille_view', $data);
			}
		}
	}

	public function del($id) {
		$this->familles_model->delSousFamille($id);
		redirect('familles/sousFamille', 'refresh');
	}

}
