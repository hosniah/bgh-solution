<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Famille extends CI_Controller {

	public function __construct() {
		parent::__construct();
//		if (!$this->session->userdata('user_lvl')) {
//			die(redirect(base_url() . 'main/noaccess'));
//		}
		$this->load->model('familles/familles_model');
	}

	public function index() {
		$data = array();
		$data['familles'] = $this->familles_model->getList();
		$this->load->view('familles/familles_view', $data);
	}

	public function edit($id) {
		$data = array();
		if ($id == 0) {
			// add new	
			if ($this->input->post('sent')) {
				$this->familles_model->addFamille($this->input->post('label'), $this->input->post('ref'));
				redirect('familles/famille', 'refresh');
			} else {
				$data['familles'] = $this->familles_model->getList();
				$this->load->view('familles/edit_view', $data);
			}
		} else {
			// edit
			if ($this->input->post('sent')) {
				$this->familles_model->updateFamille($id, $this->input->post('label'), $this->input->post('ref'));
				redirect('familles/famille', 'refresh');
			} else {
				$data['familles'] = $this->familles_model->getFamille($id);
				$data['list'] = $this->familles_model->getList();
				$this->load->view('familles/edit_view', $data);
			}
		}
	}

	public function del($id) {
		$this->familles_model->delFamille($id);
		redirect('familles/famille', 'refresh');
	}

}
