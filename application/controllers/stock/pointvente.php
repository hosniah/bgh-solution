<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Pointvente extends CI_Controller {

	public function __construct() {
		parent::__construct();
//		if (!$this->session->userdata('user_lvl')) {
//			die(redirect(base_url() . 'main/noaccess'));
//		}
		$this->load->model('stock/pointvente_model');
                $this->load->model('users/manage_model');
	}

        public function index() {
		$array = array();
		$array['listes'] = $this->pointvente_model->getList();
                foreach ($array['listes'] as $key => $value) {
                    
                    $array['listes'][$key]['resp_pv'] = $this->getUserById($value['resp_pv']);
                }
                
		$this->load->view('stock/index_view', $array);
	}


        public function getUserById($id) {
                $table = $this->manage_model->getUser($id);
                
                return $table['login'];
        }

	public function edit($id) {
		$data = array();
		if ($id == 0) {
			// add new	
			if ($this->input->post('sent')) {
				$this->pointvente_model->addpointvente($this->input->post('label'), $this->input->post('status'));
				redirect('stock/pointvente', 'refresh');
			} else {
				$data['liste'] = $this->pointvente_model->getList();
				$this->load->view('stock/edit_view', $data);
			}
		} else {
			// edit
			if ($this->input->post('sent')) {
				$this->pointvente_model->updatepointvente($id, $this->input->post('label'), $this->input->post('status'));
				redirect('stock/pointvente', 'refresh');
			} else {
				$data['pointvente'] = $this->pointvente_model->getpointvente($id);
				$data['liste'] = $this->pointvente_model->getList();
				$this->load->view('stock/edit_view', $data);
			}
		}
	}

	public function del($id) {
		$this->pointvente_model->delpointvente($id);
		redirect('stock/pointvente', 'refresh');
	}

}
