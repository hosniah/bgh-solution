<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Commande extends CI_Controller {

	public function __construct() {
		parent::__construct();
		//		if (!$this->session->userdata('user_lvl')) {
		//			die(redirect(base_url() . 'main/noaccess'));
		//		}
		$this->load->model('stock/commande_model');
        $this->load->model('warehouse/order_model');
        $this->load->model('warehouse/library_model');
	}

	public function index() {
		$data = array();
		$data['commandes'] = $this->commande_model->getCommande($this->session->userdata('user_id'));
		$this->load->view('stock/commande_view', $data);
	}
        
	public function details($id) {
			$data = array();
			$data['details'] = $this->commande_model->getListCommande($id);
			foreach ($data['details'] as $key => $product) {
			   $tty = $this->library_model->getProductColisage($product['reference']);
			   $data['details'][$key]['colisage'] = $tty[0]->{'colisage'};
			}

			$data['cmd_originale_details'] = array();
			$original_cmd = $this->order_model->getOriginalListCommande($id);
			if (!empty($original_cmd)) {
				foreach ($original_cmd as $key => $product) {
					/*var_dump($product['reference']);
					var_dump($product['quantite']);
					var_dump( $data['details'][$key]['reference']);
					var_dump( $data['details'][$key]['quantite']); */
					if ($product['reference'] == $data['details'][$key]['reference'] && $product['quantite'] != $data['details'][$key]['quantite']) {
						// update $product with the missing quantity of pcs if original quantite is greater than delivered quantity:
						$delta_quantity = $product['quantite'] - $data['details'][$key]['quantite'];
						//var_dump($delta_quantity);
						if ($delta_quantity > 0) {
							$product['quantite']             = $delta_quantity;
							$data['cmd_originale_details'][] = $product;
						}
					}
				}
			}
			//var_dump( $data['cmd_originale_details']);
			//$data['cmd_originale_details'] = $this->order_model->getOriginalListCommande($id);
			
			$this->compareOriginalAndModifiedCommand($id);
			
			$this->load->view('stock/details_view', $data);
	}

    public function compareOriginalAndModifiedCommand($id){
		$someFiles  = $this->commande_model->getListCommande($id);
		$removeDirs = $this->order_model->getOriginalListCommande($id);
		$someFiles = array_diff($someFiles, $removeDirs); 
		  var_dump($someFiles);

		foreach($someFiles AS $key => $thisFile) {
		    var_dump($thisFile);
		    var_dump($removeDirs[$key]);
		}
	}

        public function add() {
                if(!empty($this->input->post('id'))) {
                    $id = $this->input->post('id');

                    // data table order_products
                    $data = array();
                    $data['products'] = $this->commande_model->getListCommande($id);

                    //data table products
                    $validate = array();
                    $i = 0;
                    foreach ($data['products'] as $key => $value) {
                            $ref[$key] = $value['reference'];
                            $validate['listes'][$i] = $this->library_model->getProductValid($id, $ref[$key]);
                            $i++;
                    }

                    $val = array();
                    $j = 0;
                    foreach($validate['listes'] as $key => $val) {
                        $ref = $val[0]['reference'];
                        $qte = $val[0]['quantite'];
                        $this->valid_product_model->updateValidCommandProduct($ref, $qte);
                    }
                    $this->valid_product_model->updateValidCommand($id);

                    redirect('stock/commande');
                }
        }


	public function deleteProduct() {
		$id = $this->uri->segment(4); 

        if(!empty($id)) {
            var_dump($id);
        }
	}

		public function updateProduct() {
			//$id = $this->uri->segment(4); commande_id
			if(!empty($this->input->post('commande_id'))) {
				$commande_id = $this->input->post('commande_id');
				//var_dump($commande_id);

			} else {
				redirect(base_url());
			}
			if(!empty($this->input->post('article_id'))) {
				$article_id = $this->input->post('article_id');
				//var_dump($article_id);
			} else {
			    redirect(base_url());
			}
			if(!empty($this->input->post('quantite_ctn'))) {
				$quantite_ctn = $this->input->post('quantite_ctn');
				var_dump($quantite_ctn);
			}
			if(!empty($this->input->post('quantite_pcs'))) {
				$quantite_pcs = $this->input->post('quantite_pcs');
				//var_dump($quantite_pcs);
			}
			if(!empty($this->input->post('quantite_pcs'))) {
				$quantite_pcs = $this->input->post('quantite_pcs');
				//var_dump($quantite_pcs);
			}

			if (!empty($commande_id) && !empty($quantite_pcs) && !empty($article_id)) {
					$this->order_model->updateOrderProductQuantity($article_id, $quantite_pcs);
					redirect('stock/commande/details/'.$commande_id, 'refresh');
			}

		}

		public function depot_valider_commande() {
//		    if (!$this->session->userdata('admin_lvl')) {
//			die(redirect(base_url() . 'main/noaccess'));
//			} else {
				$id = $this->uri->segment(4);
				if (!empty($id)) {
					$this->commande_model->depotValidateCommand($id);
				}
				//var_dump($id);
				redirect(base_url());
		   // }
				
        }

	public function depot_completer_commande() {
				$id_commande_parent = $this->input->post('id_commande');
				
				$parent_item = $this->commande_model->getCommandeById($id_commande_parent);
				$currentDateTime = date('Y-m-d');
				$user_id       = $parent_item[0]['id_client'];
				$size_commande = $this->input->post('size_commande');
				if (!empty($id_commande_parent)) {
					$new_order    = array("status" => 100, "parent_id" =>$id_commande_parent ,"id_client" => $user_id, "date_commande" => $currentDateTime,"description"=> 'Commande complémentaire de la commande numero '.$id_commande_parent, "deleted" => 0);
			        $this->order_model->createOrder($new_order);
			        $createdOrder = $this->order_model->getInsertID();
		            $order_id     = $createdOrder[0]['MAX(id)'];
					var_dump($order_id);

				
					for ($i=0; $i<=$size_commande; $i++) {
						var_dump($this->input->post('reference_'.$i));
						var_dump($this->input->post('quantite_'.$i));
						//var_dump($this->input->post('reference_'.$i));
					}
				}
				//redirect(base_url());
	}
}