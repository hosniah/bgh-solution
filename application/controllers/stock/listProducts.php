<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ListProducts extends CI_Controller {

    public function __construct() {
        parent::__construct();
//	if (!$this->session->userdata('user_lvl')) {
//            die(redirect(base_url() . 'main/noaccess'));
//	}
            $this->load->model('warehouse/library_model');
            $this->load->model('warehouse/log_model');
            $this->load->model('familles/familles_model');
            
            $this->load->model('stock/stock_model');
	}

	public function index() {
		$data = array();
		$family=NULL; $sub_family=NULL;
		$data['prix_gros_min'] = ''; 
		$data['prix_gros_max'] = ''; $data['sub_family'] = ''; $data['family'] = '';$data['stock_pcs'] ='';$data['colisage'] = '';
		$colisage_ctn=NULL; $stock_pcs=NULL;

			$data['labelFamille'] = $this->familles_model->getList();
			$data['sousfamilles'] = $this->familles_model->getSousList();
			if(!empty($this->input->post('sousFamilles'))) {
				$data['addressbook'] = $this->stock_model->getProductFamily($this->input->post('sousFamilles'), $this->session->userdata('user_id'));
			} else {
				$data['addressbook'] = $this->stock_model->get_list($this->session->userdata('user_id'));
			}
        if(!empty($this->input->post('familles'))) {
			$data['family'] = $this->input->post('familles');
		}
        if(!empty($this->input->post('sousFamilles'))) {
			$data['sub_family'] = $this->input->post('sousFamilles');
		}
        if(!empty($this->input->post('prix_min'))) {
			 $data['prix_gros_min'] = $this->input->post('prix_min');
		}
        if(!empty($this->input->post('prix_max'))) {
			$data['prix_gros_max'] = $this->input->post('prix_max');
		}
        if(!empty($this->input->post('stock_pcs'))) {
			$data['stock_pcs'] = $this->input->post('stock_pcs');
		}
        if(!empty($this->input->post('colisage'))) {
			$data['colisage'] = $this->input->post('colisage');
		}

		$data['addressbook'] = $this->stock_model->load($this->session->userdata('user_id'),$data['family'], $data['sub_family'], $data['prix_gros_min'] , $data['prix_gros_max'], $data['colisage'], $data['stock_pcs']);
			$this->load->view('stock/dashboard_view', $data);
	}
}
