<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('warehouse/log_model');
		$this->load->model('warehouse/library_model');
		$this->load->model('warehouse/packing_model');
		$this->load->model('users/manage_model');
		$this->load->model('warehouse/order_model');
	}

	public function index() {
		$data = array();
		if ($my_id = $this->session->userdata('user_id')) {
			//$id_boutique = $this->manage_model->getUserPtDeVente($my_id);
		//	var_dump($this->session->userdata('id_pointvente'));
			
			$data['orders'] = $this->order_model->getList();
			//var_dump($data['orders'] );
			 if (($this->session->userdata('admin_lvl')) ||
					($this->session->userdata('superadmin_lvl')) ||
					($this->session->userdata('responsabledepot_lvl'))) {
				$this->load->view('admin_orders_reports_view', $data);
			} else {
				redirect('vente/vente');
			}
		} else {
			$this->load->view('login', $data);
		}
	}

	public function noaccess() {
		$data = array();
		$this->load->view('noaccess_view', $data);
	}
	
	public function version($requestor) {
		if($requestor != '') {
			// DDOS SEC log
			// log_message('error', 'REQ ' . $requestor);
			echo VERSION;
		}
	}
}
