<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Pointvente_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getList() {
		$query = $this->db->query("SELECT * FROM pointvente WHERE deleted_pv='0' ORDER BY label_pv ASC");
		$points = array();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$point = array();
				$point['id_pv'] = $row->id_pv;
				$point['label_pv'] = $row->label_pv;
				$point['tel_pv']	 = $row->tel_pv;
				$point['fax_pv']	 = $row->fax_pv;
				$point['adresse_pv']	 = $row->adresse_pv;
				$point['email_pv']	 = $row->email_pv;
				$point['resp_pv']	 = $row->resp_pv;
				$point['deleted_pv'] = $row->deleted_pv; 
				$points[] = $point;
			}
		}

		return $points;
	}

	public function getpointvente($id) {
		$query = $this->db->query("SELECT * FROM pointvente WHERE id_pv='$id'");
		$points = array();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$point['id_pv']      = $row->id_pv;
				$point['label_pv']   = $row->label_pv;
				$point['tel_pv']	 = $row->tel_pv;
				$point['fax_pv']	 = $row->fax_pv;
				$point['adresse_pv'] = $row->adresse_pv;
				$point['email_pv']	 = $row->email_pv;
				$point['resp_pv']	 = $row->resp_pv;
				$point['deleted_pv'] = $row->deleted_pv; 
				$points[]            = $point;
			}
		}

		return $points;
	}
        
        public function getpointventeResp($id) {
		$query = $this->db->query("SELECT * FROM pointvente WHERE resp_pv='$id'");
		$points = array();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$point['id_pv']      = $row->id_pv;
				$point['label_pv']   = $row->label_pv;
				$point['tel_pv']	 = $row->tel_pv;
				$point['fax_pv']	 = $row->fax_pv;
				$point['adresse_pv'] = $row->adresse_pv;
				$point['email_pv']	 = $row->email_pv;
				$point['resp_pv']	 = $row->resp_pv;
				$point['deleted_pv'] = $row->deleted_pv; 
				$points[]            = $point;
			}
		}

		return $points;
	}

	public function addpointvente($label, $tel, $fax, $adresse, $email, $resp) {
		$this->db->query("INSERT INTO pointvente (label_pv, tel_pv, fax_pv, adresse_pv, email_pv, resp_pv, deleted_pv) VALUES ('$label', '$tel', '$fax', '$adresse', '$email', '$resp', '0')");
	}

	public function updatepointvente($id, $label, $tel, $fax, $adresse, $email, $resp, $status) {
		$this->db->query("UPDATE pointvente SET label_pv='$label', tel_pv='$tel', fax_pv='$fax', adresse_pv='$adresse', email_pv='$email', resp_pv='$resp', deleted_pv='$status' WHERE id_pv='$id'");
	}

	public function delpointvente($id) {
		$this->db->query("UPDATE pointvente SET deleted_pv='1' WHERE id_pv='$id'");
	}
	
	public function getStockPointVente($id) {

	}

}
