<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Stock_model extends CI_Model {
    
    function __construct() {
        parent::__construct(); 
        $this->load->model('warehouse/library_model');
				$this->load->model('familles/familles_model');
    }
    
    function load($id, $family, $sub_family, $prix_gros_min, $prix_gros_max, $colisage_ctn, $stock_pcs) {  
		//='$id';
                $this->db->select('*');
		if(!empty($family)) {
			$this->db->where('nsFamille', $family);
		}
		if(!empty($sub_family)) {
			$this->db->where('nsFamille', $sub_family);
		}
		if(!empty($prix_gros_min)) {
			$this->db->where('prixGrosHT > '.$prix_gros_min);
		}
		if(!empty($prix_gros_max)) {
			$this->db->where('prixGrosHT <  '.$prix_gros_max);
		}
		if(!empty($colisage_ctn)) {
			$this->db->where('colisageCtn',$colisage_ctn);
		}
		if(!empty($stockPcs)) {
			$this->db->where('stockPcs >= '.$stock_pcs);
		}

        $query = $this->db->query("SELECT * FROM pointventestock WHERE id_pointvente='$id' AND STATUS='200'");
        $products = array();
		if ($query->num_rows() > 0) {
			
			
			foreach ($query->result() as $row) {
                                $product['image']        = $row->image;
                                $product['id_importateur'] = $row->id_importateur;
                                $product['nsFamille']    = $row->nsFamille;
                                $product['reference']    = $row->reference;
                                $product['codeBarre']    = $row->codeBarre;
                                $product['designation']  = $row->designation;
                                $product['prixGrosHT']   = $row->prixGrosHT;
                                $product['tva']          = $row->tva;
                                $product['unite']        = $row->unite;
                                $product['colisageCtn']  = $row->colisageCtn;
                                $product['colisagePacket']  = $row->colisagePacket;
                                $product['emballage']    = $row->emballage;
                                $product['prixHlif']     = $row->prixHlif;
                                $product['prixTunis']    = $row->prixTunis;
                                $product['ctnDispo']     = $row->ctnDispo;
                                $product['pcsDispo']     = $row->pcsDispo;
                                $product['stockPcs']     = $row->stockPcs;
                                $product['cf']           = $row->cf;
                                $product['deleted']      = 0;
                                $products[]              = $product;
			}
		}
		return $products;
    }

    function get_list($id) {
        $query    = $this->db->query("SELECT * FROM pointventestock WHERE id_pointvente ='$id' AND STATUS='200'");
        $products = array();
        if ($query->num_rows() > 0) {
            $product['image']        = $row->image;
            $product['nsFamille']    = $row->nsFamille;
            $product['reference']    = $row->reference;
            $product['codeBarre']    = $row->codeBarre;
            $product['designation']  = $row->designation;
            $product['prixGrosHT']   = $row->prixGrosHT;
            $product['tva']          = $row->tva;
            $product['unite']        = $row->unite;
            $product['colisageCtn']  = $row->colisageCtn;
            $product['emballage']    = $row->emballage;
            $product['prixHlif']     = $row->prixHlif;
            $product['prixTunis']    = $row->prixTunis;
            $product['ctnDispo']     = $row->ctnDispo;
            $product['pcsDispo']     = $row->pcsDispo;
            $product['stockPcs']     = $row->stockPcs;
            $product['cf']           = $row->cf;
            $product['deleted']      = 0;
            $products[]              = $product;
        }
	return $products;
    }

    public function getProductFamily($id, $pt) {
		$query = $this->db->query("SELECT * FROM pointventestock WHERE nsFamille='$id' AND id_pointvente='$pt' AND STATUS='200'");
		$products = array();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$product['image']        = $row->image;
				$product['nsFamille']    = $row->nsFamille;
				$product['reference']    = $row->reference;
				$product['codeBarre']    = $row->codeBarre;
				$product['designation']  = $row->designation;
				$product['prixGrosHT']   = $row->prixGrosHT;
				$product['tva']          = $row->tva;
				$product['unite']        = $row->unite;
				$product['colisageCtn']  = $row->colisageCtn;
				$product['emballage']    = $row->emballage;
				$product['prixHlif']     = $row->prixHlif;
				$product['prixTunis']    = $row->prixTunis;
				$product['ctnDispo']     = $row->ctnDispo;
				$product['pcsDispo']     = $row->pcsDispo;
				$product['stockPcs']     = $row->stockPcs;
				$product['cf']           = $row->cf;
				$product['deleted']      = 0;
				$products[]              = $product;
			}
		}

		return $products;
	}

	public function updatepointDeVenteStock($ref, $qte, $id_pointvente, $id_importateur=NULL, $nsFamille=NULL ,$status=NULL,  $codeBarre = 0, $designation=0, $prixGrosHT=0, $tva=0, $unite=0, $colisageCtn=0, $emballage=0, $prixHlif= 0, $prixTunis=0, $ctnDispo=0, $pcsDispo=0, $stockPcs=0, $cf=0, $image="NULL"){

/*  

      $this->db->query("INSERT INTO pointventestock (reference, stockPcs , id_pointvente , codeBarre, designation,
                                  prixGrosHT, tva,unite, colisageCtn, emballage, prixHlif,
                                  prixTunis, ctnDispo,pcsDispo, cf, image,id_importateur, nsFamille ,status) VALUES ('$ref','$qte', '$id_pointvente', 
		'$codeBarre', '$designation',
		'$prixGrosHT', '$tva',
		'$unite', '$colisageCtn',
		'$emballage', '$prixHlif',
		'$prixTunis', '$ctnDispo',
		'$pcsDispo'
		'$cf', '$image', '$id_importateur', '$nsFamille' ,'$status'
		)");

*/

 $this->db->query("
INSERT INTO `warehouse_ghassen`.`pointventestock` (`id_importateur`, `nsFamille`, `reference`, `codeBarre`, `designation`, `prixGrosHT`, `tva`, `unite`, `colisageCtn`, `emballage`, `prixHlif`, `prixTunis`, `ctnDispo`, `pcsDispo`, `stockPcs`, `cf`, `status`, `image`, `id_pointvente`)
 VALUES ('1', '2', '$ref', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$qte', NULL, NULL, NULL, '$id_pointvente')");


	}

	function get_product_list_by_pt_de_vente($id, $family, $sub_family, $prix_gros_min, $prix_gros_max, $colisage_ctn, $stock_pcs) {

        $this->db->select('*');
            if(!empty($family)) {
			$this->db->where('nsFamille', $family);
		}
		if(!empty($sub_family)) {
			$this->db->where('nsFamille', $sub_family);
		}
		if(!empty($prix_gros_min)) {
			$this->db->where('prixGrosHT > CAST('.$prix_gros_min.' AS UNSIGNED)');
		}
		if(!empty($prix_gros_max)) {
			$this->db->where('prixGrosHT <  CAST('.$prix_gros_max.' AS UNSIGNED)');
		}
		if(!empty($colisage_ctn)) {
			$this->db->where('colisageCtn',$colisage_ctn);
		}
		if(!empty($stockPcs)) {
			$this->db->where('stockPcs >= '.$stock_pcs);
		}
                
                $this->db->where('id_pointvente = '.$id);    
                $query = $this->db->get('pointventestock'); 

		$points = array();
		if ($query->num_rows() > 0) {
                    foreach ($query->result() as $row) {
												$array = $this->library_model->getProductFromReference($row->reference);
												$table = $this->familles_model->getSousListId($array[0]->nsFamille);
												//var_dump($table);die;
                        $point['id'] = $row->id;
                        $point['id_importateur'] = $row->id_importateur; 
                        $point['nsFamille'] = $table[0]['label_famille'].'/'.$table[0]['labelSousFamille'];
                        $point['reference'] = $row->reference;
                        $point['codeBarre'] = $array[0]->codeBarre;
                        $point['designation'] = $array[0]->designation;
                        $point['prixGrosHT'] = $array[0]->prixGrosHT;
                        $point['colisageCtn'] = $array[0]->colisageCtn;
												$point['colisagePacket'] = $array[0]->colisagePacket;
                        $point['emballage'] = $array[0]->emballage;
                        $point['prixHlif'] = $array[0]->prixHlif;
                        $point['prixTunis'] = $array[0]->prixTunis;
                        $point['ctnDispo'] = $array[0]->ctnDispo;
                        $point['pcsDispo'] = $array[0]->pcsDispo;
                        $point['stockPcs'] = $array[0]->stockPcs;
                        $points[]              = $point;
                    }
		}
		return $points;
	}

    public function getProductStockByPtDeVente($ref, $ptDeVente) {
		$query = $this->db->query("SELECT SUM(stockPcs) AS stock_total FROM pointventestock WHERE reference = '$ref' and id_pointvente='$ptDeVente'");
		return $query->result();
	}

}
