<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Sales_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getVentesByPointDeVenteId($id_pt_vente){
	
        $this->db->select('*');
        $this->db->where('point_vente_id', $id_pt_vente);
        $this->db->where('is_active', 1);
        $this->db->from('sales');
        $query = $this->db->get();
		return $query->result_array();
	}

	public function addVenteGros($data){
		$insert = $this->db->insert('sales', $data);  // affiche INSERT INTO `sales` (`point_vente_id`, `user_id`, `time`, `sum`, `type_vente`) VALUES (0, '1', '2016-03-28 10:21:37', 0, 1)
	    return $this->db->insert_id();
	}

	public function enforceVenteAsDetail($id) {
		$this->db->query("UPDATE sales SET type_vente='2' WHERE sale_id='$id'");
	}

        // les vente details sont type_vente=2
	public function countVenteDetail($id_pt_vente) {
             $this->db->select('count(sale_id) as nb_ventes');
             $this->db->where('point_vente_id', $id_pt_vente);
             $this->db->where('type_vente', 2);
             $this->db->from('sales');
             $query = $this->db->get();
	     return $query->result_array();
	}
	
	public function countVenteGros($id_pt_vente) {
             $this->db->select('count(sale_id) as nb_ventes');
             $this->db->where('point_vente_id', $id_pt_vente);
             $this->db->where('type_vente', 1);
             $this->db->from('sales');
             $query = $this->db->get();
	     return $query->result_array();
	}
       
	public function addSaleDetailItem($data) {
	    $insert = $this->db->insert('sale_details', $data);
	    return $this->db->insert_id();
	}
        
        public function deleteSaleDetailItem($id) {
                $this->db->where('sd_id', $id);
		$this->db->delete('sale_details');
	}

	    /**
    * Get ^product by its reference
    * @param int $ref 
    * @return array
    */
    public function get_product_details_by_ref($ref) {
        $this->db->select('*');
      //  $this->db->from('pointventeproducts'); with check on point vente id also
        $this->db->from('products');
        $this->db->where('reference', $ref);
        $query = $this->db->get();
        return $query->result_array();
    }
	
	public function get_product_details_by_barcode($barcode) {
        $this->db->select('*');
      //  $this->db->from('pointventeproducts'); with check on point vente id also
        $this->db->from('products');
        $this->db->where('codeBarre', $barcode);
        $query = $this->db->get();
        return $query->result_array();
	}

    public function get_product_refs_for_autocomplete() {
        $this->db->select('*');
      //  $this->db->from('pointventeproducts'); with check on point vente id also
        $this->db->from('products');
        $this->db->group_by("reference"); 
        $query = $this->db->get();
		return $query->result_array();
		//return json_encode($query, 128);
       // return $query->result_array();
    }

    public function lod_sales_details($id_sale) {
        $this->db->select('*');
        $this->db->from('sale_details');
        $this->db->where('sale_id', $id_sale);
        $query = $this->db->get();
        return $query->result_array();

    }


	public function getCommande($id) {
		$query = $this->db->query("SELECT distinct(id_commande), status FROM order_products_client WHERE id_pointVente = '$id' AND status = '100' ORDER BY id DESC");

		$orders = array();

		if ($query->num_rows() > 0) {

			foreach ($query->result() as $row) {
				$order = array();
				$order['id_commande'] = $row->id_commande;
                                $order['status']	 = $row->status;
				$orders[] = $order;
			}
		}

		return $orders;
	}

	public function updateCommandeTTC($id, $sum) {
		$this->db->query("UPDATE sales SET sum='$sum' WHERE sale_id='$id'");
	}
	
	public function validateSale($id) {
		$this->db->query("UPDATE sales SET is_active='1' WHERE sale_id='$id'");
	}

	public function updateArticlePrice($id, $price) {
		$this->db->query("UPDATE sale_details SET current_price='$price' WHERE sd_id='$id'");
	}
	
	public function updateArticleQte($id, $qte) {
		$this->db->query("UPDATE sale_details SET quantity='$qte' WHERE sd_id='$id'");
	}

	public function getListCommande($id) {
	$query = $this->db->query("SELECT * FROM order_products_client WHERE id_commande='$id'");
	$orders = array();
	if ($query->num_rows() > 0) {
		foreach ($query->result() as $row) {
			$order = array();
			$order['id'] = $row->id;
			$order['id_commande'] = $row->id_commande;
			$order['reference']	 = $row->reference;
			$order['quantite']	 = $row->quantite;
			$order['commentaires']	 = $row->commentaires;
							$order['id_pointVente']	 = $row->id_pointVente;
							$order['status']	 = $row->status;
							$orders[]                = $order;
		}
	}

	return $orders;
}



}
