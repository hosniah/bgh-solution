<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Facture_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function add($data){
		$insert = $this->db->insert('facture', $data);
	    return $this->db->insert_id();
	}

}
