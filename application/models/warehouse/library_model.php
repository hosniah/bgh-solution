<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Library_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	function load($family, $sub_family, $prix_gros_min, $prix_gros_max, $colisage_ctn, $stock_pcs) {  
		//='$id';
		/*
		if(!empty($family)) {
			$this->db->where('nsFamille', $family);
		}
		*/
		if(!empty($sub_family)) {
			$this->db->where('nsFamille', $sub_family);
		}
		if(!empty($prix_gros_min)) {
			$this->db->where('prixGrosHT > '.$prix_gros_min);
		}
		if(!empty($prix_gros_max)) {
			$this->db->where('prixGrosHT <  '.$prix_gros_max);
		}
		if(!empty($colisage_ctn)) {
			$this->db->where('colisageCtn',$colisage_ctn);
		}
		if(!empty($stockPcs)) {
			$this->db->where('stockPcs >= '.$stock_pcs);
		}

        $query = $this->db->get('products');
		$products = array();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
                                $product['image']        = $row->image;
                                $product['id_importateur'] = $row->id_importateur;
                                $product['nsFamille']    = $row->nsFamille;
                                $product['reference']    = $row->reference;
                                $product['codeBarre']    = $row->codeBarre;
                                $product['designation']  = $row->designation;
                                $product['prixGrosHT']   = $row->prixGrosHT;
                                $product['tva']          = $row->tva;
                                $product['unite']        = $row->unite;
                                $product['colisageCtn']  = $row->colisageCtn;
                                $product['colisagePacket']  = $row->colisagePacket;
                                $product['emballage']    = $row->emballage;
                                $product['prixHlif']     = $row->prixHlif;
                                $product['prixTunis']    = $row->prixTunis;
                                $product['ctnDispo']     = $row->ctnDispo;
                                $product['pcsDispo']     = $row->pcsDispo;
                                $product['stockPcs']     = $row->stockPcs;
                                $product['cf']           = $row->cf;
                                $product['deleted']      = 0;
                                $products[]              = $product;
			}
		}
		return $products;
    }

	public function getList() {
		$query = $this->db->query("SELECT * FROM products WHERE deleted='0' ORDER BY reference ASC");
		$products = array();
		if ($query->num_rows() > 0) {

			foreach ($query->result() as $row) {
				$product = array();
				$product['id'] = $row->id;
                                $product['image'] = $row->image;
                                $product['nsFamille'] = $row->nsFamille; 
                                $product['reference'] = $row->reference; 
                                $product['codeBarre'] = $row->codeBarre; 
                                $product['designation'] = $row->designation; 
                                $product['prixGrosHT'] = $row->prixGrosHT; 
                                $product['tva'] = $row->tva; 
                                $product['unite'] = $row->unite; 
                                $product['colisageCtn'] = $row->colisageCtn; 
                                $product['emballage'] = $row->emballage; 
                                $product['prixHlif'] = $row->prixHlif; 
                                $product['prixTunis'] = $row->prixTunis; 
                                $product['ctnDispo'] = $row->ctnDispo; 
                                $product['pcsDispo'] = $row->pcsDispo; 
                                $product['stockPcs'] = $row->stockPcs; 
                                $product['cf'] = $row->cf; 
                                $product['deleted'] = $row->deleted; 
                                $products[] = $product;
			}
		}

		return $products;
	}
        
    public function getProductFamily($id) {
		$query = $this->db->query("SELECT * FROM products WHERE nsFamille='$id'");
		$products = array();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
                                
                                $product['image']        = $row->image;
                                $product['nsFamille']    = $row->nsFamille;
                                $product['reference']    = $row->reference;
                                $product['codeBarre']    = $row->codeBarre;
                                $product['designation']  = $row->designation;
                                $product['prixGrosHT']   = $row->prixGrosHT;
                                $product['tva']          = $row->tva;
                                $product['unite']        = $row->unite;
                                $product['colisageCtn']  = $row->colisageCtn;
                                $product['emballage']    = $row->emballage;
                                $product['prixHlif']     = $row->prixHlif;
                                $product['prixTunis']    = $row->prixTunis;
                                $product['ctnDispo']     = $row->ctnDispo;
                                $product['pcsDispo']     = $row->pcsDispo;
                                $product['stockPcs']     = $row->stockPcs;
                                $product['cf']           = $row->cf;
                                $product['deleted']      = 0;
                                $products[]              = $product;
			}
		}

		return $products;
	}
        
        
    public function getProductIdFromReference($ref) {
		$query = $this->db->query("SELECT id FROM products WHERE reference = '$ref'");
		return $query->result();
	}
        

	public function getPackingsList() {
		$query = $this->db->query("SELECT * FROM packings WHERE deleted='0' ORDER BY name ASC");

		$products = array();

		if ($query->num_rows() > 0) {

			foreach ($query->result() as $row) {
				$product = array();
				$product['id'] = $row->id;
				$product['name'] = $row->name;
				$product['desc'] = $row->description;
				$products[] = $product;
			}
		}

		return $products;
	}

	public function addProduct($name, $desc) {
		$this->db->query("INSERT INTO products (name, description, deleted) VALUES ('$name', '$desc', 0)");
	}
	
	public function addProductStock($ref, $stockPCS) {
		$this->db->query("INSERT INTO products (reference, stockPcs ) VALUES ('$ref','$stockPCS')");
	}
	
	public function updateProduct($id, $name, $desc) {
		$this->db->query("UPDATE products SET name='$name', description='$desc' WHERE id='$id'");
	}

	public function updateProductStock($ref, $stockPCS) {
		$this->db->query("UPDATE products SET stockPcs='$stockPCS' WHERE reference='$ref'");
	}



	public function getProduct($id) {
		$query = $this->db->query("SELECT * FROM products WHERE id='$id'");
		$product = array();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$product['id'] = $row->id;
                                $product['image'] = $row->image;
                                $product['nsFamille'] = $row->nsFamille; 
                                $product['reference'] = $row->reference; 
                                $product['codeBarre'] = $row->codeBarre; 
                                $product['designation'] = $row->designation; 
                                $product['prixGrosHT'] = $row->prixGrosHT; 
                                $product['tva'] = $row->tva; 
                                $product['unite'] = $row->unite; 
                                $product['colisageCtn'] = $row->colisageCtn; 
                                $product['emballage'] = $row->emballage; 
                                $product['prixHlif'] = $row->prixHlif; 
                                $product['prixTunis'] = $row->prixTunis; 
                                $product['ctnDispo'] = $row->ctnDispo; 
                                $product['pcsDispo'] = $row->pcsDispo; 
                                $product['stockPcs'] = $row->stockPcs; 
                                $product['cf'] = $row->cf; 
                                $product['deleted'] = $row->deleted; 
				}
		}

		return $product;
	}

	public function delProduct($id) {
		$this->db->query("UPDATE products SET deleted='1' WHERE id='$id'");
	}

	public function getDescriptions() {
		$query = $this->db->query("SELECT DISTINCT description FROM products WHERE deleted=0");
		$products = array();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$product['desc'] = $row->description;
				$products[] = $product;
			}
		}
		return $products;
	}
        
        public function getProductValid($id, $ref) {
            $query = $this->db->query("SELECT * FROM order_products WHERE id_commande= '$id' and reference = '$ref' ORDER BY id ASC");
            $products = array();
            if ($query->num_rows() > 0) {

                    foreach ($query->result() as $row) {
                            $product = array();
                            $product['quantite'] = $row->quantite;
                            $product['reference'] = $row->reference;
                            $products[] = $product;
                    } 
            }
            return $products;
        }
 
    public function getProductStock($ref) {
		$query = $this->db->query("SELECT SUM(stockPcs) AS stock_total FROM products WHERE reference = '$ref'");
		return $query->result();
	}

    public function getProductColisage($ref) {
		$query = $this->db->query("SELECT colisageCtn AS colisage FROM products WHERE reference = '$ref'");
		return $query->result();
	}
        
        public function getProductFromReference($ref) {
		$query = $this->db->query("SELECT * FROM products WHERE reference = '$ref'");
		return $query->result();
	}

}
