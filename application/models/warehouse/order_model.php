<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Order_model extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->model('users/manage_model');
		$this->load->model('stock/pointvente_model');
	}

	public function getList($id=NULL) {
		if (!empty($id)) {
			$this->db->where('id_client', $id);
		}
		$this->db->where('deleted', 0);
		$this->db->select('*');
                $this->db->from('orders');
		$query = $this->db->get();
		$products = array();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$product              = array();
				$product['id']        = $row->id;
				//$product['id_client'] = $row->id_client;
				$product['date_commande']  = $row->date_commande;
				switch($row->status) {
					case 100:
						$product['status']         = "Nouvelle commande";
					break;
					case 200:
						$product['status']         = "Commande Validée";
					break;
					case 300:
						$product['status']         = "Commande Rejetée";
					break;
					default:
						$product['status']         = "Nouvelle commande";
					break;
				}
				//$product['status']         = $row->status;
				$product['description']    = $row->description;
				$table = $this->manage_model->getUserLogin($row->id_client);
				$product['id_client'] = $table['login'];
				
				$array = $this->pointvente_model->getpointvente($row->id_point_vente);
				$product['id_point_vente']    = $array[0]['label_pv'];
				$product['deleted']        = $row->deleted;
				$products[]                = $product;
			}
		}
		return $products;
	}

	public function getListCommmandForAgenDepotByStatus($status) {
		if (!empty($status)) {
			$this->db->where('status', $status);
		}
		$this->db->where('deleted', 0);
		$this->db->select('*');
		$this->db->from('orders');
		$query = $this->db->get();
		$products = array();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$product              = array();
				$product['id']        = $row->id;

				$table = $this->manage_model->getUserLogin($row->id_client);
				$product['id_client'] = $table['login'];

				$array = $this->pointvente_model->getpointvente($row->id_point_vente);
				$product['id_point_vente']    = $array[0]['label_pv'];

				$product['date_commande']  = $row->date_commande;
				switch($row->status) {
					case 100:
						$product['status']         = "Nouvelle commande";
					break;
					case 200:
						$product['status']         = "Commande Validée";
					break;
					case 300:
						$product['status']         = "Commande Rejetée";
					break;
					default:
						$product['status']         = "Nouvelle commande";
					break;
				}
				//$product['status']         = $row->status;
				$product['description']    = $row->description;
				$product['deleted']        = $row->deleted;
				$products[]                = $product;
			}
		}
		return $products;
	}

	//public function addOrder($name, $desc) {
	public function addOrder($reference, $quantite) {
		//$this->db->query("INSERT INTO orders (name, description) VALUES ('$name', '$desc')");
		$this->db->query("INSERT INTO orders (reference, description) VALUES ('$reference', '$quantite')");
	}

	public function createOrder($data) {
		$insert = $this->db->insert('orders', $data);
	    return $insert;
	}

	public function keepTrackOfOriginalOrderProducts($data) {
		$insert = $this->db->insert('order_products_client', $data);
	    return $insert;
	}

	public function getOriginalListCommande($id){
		$query = $this->db->query("SELECT * FROM order_products_client WHERE id_commande='$id'");
		$orders = array();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$order = array();
				$order['id'] = $row->id;
				$order['id_commande'] = $row->id_commande;
				$order['reference']	 = $row->reference;
				$order['quantite']	 = $row->quantite;
				$order['commentaires']	 = $row->commentaires; 
				$orders[]                = $order;
			}
		}

		return $orders;
	}

	function getInsertID() {
            $this->db->select('MAX(id)');
            $this->db->from('orders');
            $query = $this->db->get();
            return $query->result_array(); 
	}

	public function createOrderProducts($data) {
            $insert = $this->db->insert('order_products', $data);
	    return $insert;
	}

	//public function updateOrder($id, $name, $desc) {
	public function updateOrder($id, $reference, $quantite) {
		//$this->db->query("UPDATE orders SET name='$name', description='$desc' WHERE id='$id'");
		$this->db->query("UPDATE orders SET reference='$reference', quantite='$quantite' WHERE id='$id'");
	}

	public function updateOrderProductQuantity($id, $quantite) {
		$this->db->query("UPDATE order_products SET quantite='$quantite' WHERE id='$id'");
	}

	public function getOrder($id) {
            
		$query = $this->db->query("SELECT * FROM orders WHERE '$id'");
		$product = array();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
			//var_dump($row);
				$product['id']            = $row->id;
				$product['status']        = $row->status;
				$product['id_client']     = $row->id_client;
				$array = $this->pointvente_model->getpointvente($row->id_point_vente);
				$product['id_point_vente']    = $array[0]['label_pv'];
				$product['date_commande'] = $row->date_commande;
				$product['description']   = $row->description;
				$product['deleted']       = $row->deleted;
				//get commande details here
				$query2 = $this->db->query("SELECT * FROM order_products WHERE id_commande='$id'");
				$product['order_details'] = $query2->result();
						foreach($product['order_details'] as $key => $row) {
							$query = $this->db->query("SELECT colisageCtn AS colisage FROM products WHERE reference like '". $row->{'reference'}."'");
							$colisage = $query->result();
							$product['order_details'][$key]->colisage = $colisage[0]->{'colisage'} ;
							$product['order_details'][$key]->ctn = $product['order_details'][$key]->quantite / $colisage[0]->{'colisage'} ;
							//var_dump($product['order_details'][$key]);
							//var_dump( $query->result());
						}

			}
		}

		return $product;
	}
        
        public function getOrderCommandes($id, $status=null) {
            $query = $this->db->query("SELECT * FROM orders WHERE id_point_vente = '$id' and status = '$status'");
            $product = array();
            if ($query->num_rows() > 0) {
                foreach ($query->result() as $row) {
                //var_dump($row);
                    $product['id']            = $row->id;
                    $product['status']        = $row->status;
                    $product['id_client']     = $row->id_client;
                    $product['id_point_vente']     = $row->id_point_vente;
                    $product['date_commande'] = $row->date_commande;
                    $product['description']   = $row->description;
                    $product['deleted']       = $row->deleted;
                    //get commande details here
                    $query2 = $this->db->query("SELECT * FROM order_products WHERE id_commande='$row->id'");
                    $product['order_details'] = $query2->result();
                    
                    $products[] = $product;
                }
                
            }

		return $products;
	}

	public function delOrder($id) {
		$this->db->query("UPDATE orders SET deleted='1' WHERE id='$id'");
	}

}
