<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Familles_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getList() {
		$q = "SELECT * FROM famille WHERE status ='0'";
		//echo $q; exit;
		$query = $this->db->query($q);

		$familles = array();

		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$famille = array();
				$famille['id_famille']    = $row->id_famille;
				$famille['label_famille'] = $row->label_famille;
                                $famille['code_famille'] = $row->code_famille;
				$familles[] = $famille;
			}
		}


		return $familles;
	}

	public function addFamille($label, $ref) {
		$this->db->query("INSERT INTO famille (label_famille, code_famille, status) VALUES ('$label', '$ref', '0')");
	}

	public function updateFamille($id, $label, $ref) {
		$this->db->query("UPDATE famille SET label_famille='$label', code_famille='$ref' WHERE id_famille='$id'");
	}

	public function getFamille($id) {
		$query = $this->db->query("SELECT * FROM famille WHERE id_famille='$id'");
		$famille = array();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$famille = array();
				$famille['id_famille'] = $row->id_famille;
				$famille['label_famille'] = $row->label_famille;
                                $famille['code_famille'] = $row->code_famille;
				$familles[] = $famille;
			}
		}

		return $famille;
	}

	public function delFamille($id) {
		$this->db->query("UPDATE famille SET status='1' WHERE id_famille='$id'");
	}

        
        
        
        public function getSousList() {
        $q = "SELECT sousfamille.idSousFamille, 
                     sousfamille.labelSousFamille,
                     famille.label_famille FROM sousfamille, famille
                WHERE famille.id_famille = sousfamille.idFamille";
        //echo $q; exit;

        $query = $this->db->query($q);

        $sousfamilles = array();

        if ($query->num_rows() > 0) {

                foreach ($query->result() as $row) {
                        $sousfamille = array();
                        $sousfamille['idSousFamille'] = $row->idSousFamille;
                        $sousfamille['label_famille'] = $row->label_famille;
                        $sousfamille['labelSousFamille'] = $row->labelSousFamille;
                        $sousfamilles[] = $sousfamille;
                }
        }

        //var_dump($users); exit;

        return $sousfamilles;
        }
				
        public function getSousListId($id) {
        $q = "SELECT sousfamille.idSousFamille, 
                     sousfamille.labelSousFamille,
                     famille.label_famille FROM sousfamille, famille
                WHERE famille.id_famille = sousfamille.idFamille
                    AND sousfamille.idSousFamille = $id";
        //echo $q; exit;

        $query = $this->db->query($q);

        $sousfamilles = array();

        if ($query->num_rows() > 0) {

                foreach ($query->result() as $row) {
                        $sousfamille = array();
                        $sousfamille['idSousFamille'] = $row->idSousFamille;
                        $sousfamille['label_famille'] = $row->label_famille;
                        $sousfamille['labelSousFamille'] = $row->labelSousFamille;
                        $sousfamilles[] = $sousfamille;
                }
        }

        //var_dump($users); exit;

        return $sousfamilles;
        }

        public function addSousFamille($famille, $label) {
        $this->db->query("INSERT INTO sousfamille (idFamille, labelSousFamille) VALUES ('$famille', '$label')");
        }

        public function updateSousFamille($id, $famille, $label) {
        $this->db->query("UPDATE sousfamille SET idFamille='$famille', labelSousFamille='$label' WHERE idSousFamille='$id'");
        }

        public function getSousFamille($id) {
        $query = $this->db->query("SELECT * FROM sousfamille WHERE idSousFamille='$id'");
        $sousfamilles = array();
        if ($query->num_rows() > 0) {
                foreach ($query->result() as $row) {
                        $sousfamille = array();
                        $sousfamille['idSousFamille'] = $row->idSousFamille;
                        $sousfamille['idFamille'] = $row->idFamille;
                        $sousfamille['labelSousFamille'] = $row->labelSousFamille;
                        $sousfamilles[] = $sousfamille;
                }
        }

        return $sousfamilles;
        }

        public function delSousFamille($id) {
        $this->db->query("UPDATE sousfamille SET status='1' WHERE idSousFamille='$id'");
        }
}
